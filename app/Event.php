<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    
    protected $table = 'events';

    public function locations()
    {
        return $this->hasMany('App\Location');
    }

    public function source_identifiers()
    {
        return $this->hasMany('App\SourceIdentifier');
    }
}
