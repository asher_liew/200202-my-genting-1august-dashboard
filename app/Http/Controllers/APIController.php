<?php

namespace App\Http\Controllers;

use App\Event;
use App\Player;
use App\Score;
use App\SendVoucherEmailJob;
use App\SourceIdentifier;
use App\Voucher;
use App\VoucherDistribution;
use App\VoucherStock;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\VoucherDistributionsFailRequest;

class APIController extends Controller
{
    /**
     * Get the saved event ID from the cookie
     */
    public function getEventId()
    {
        $eventId = 1;
        if (isset($_COOKIE["eventID"])) {
            if ($_COOKIE["eventID"] != "") {
                $eventId = $_COOKIE["eventID"];
            }
        }

        return $eventId;
    }

    /*****************************************************
     *****************  Internal APIs  *******************
     ****************************************************/

    /**
     * Get the list of the events to apply to the dropdown list in the top navbar
     */
    public function getEventList()
    {
        $events = Event::select('id', 'event_code')->get();

        return response(json_encode($events), Response::HTTP_OK);
    }

    /**
     * Get the list of the source identifier to apply to the dropdown list in the top navbar
     */
    public function getSourceIdentifierListInternal() {
        $eventId = self::getEventId();

        $sourceIdentifiers = DB::table('source_identifiers')
                                ->join('events', 'source_identifiers.event_id', '=', 'events.id')
                                ->join('locations', 'source_identifiers.location_id', '=', 'locations.id')
                                ->select('events.event_code', 'locations.location', 'source_identifiers.code', 'source_identifiers.description')
                                ->where('source_identifiers.event_id', $eventId)
                                ->get();

        return response(json_encode($sourceIdentifiers), Response::HTTP_OK);
    }

    /*****************************************************
     *****************  External APIs  *******************
     ****************************************************/

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Player data submission API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Success
     * 2. Fail
     * 3. Duplicate
     *
     * Requires 8 parameters
     * 1. name
     * 2. email
     * 3. contact => String with country code in front of every phone number
     * 4. age => Positive Integer
     * 5. dob => Date [yyyy-mm-dd]
     * 6. gender => ["0", "male", "female", "other"]
     * 7. source_identifier_code
     * 8. created_at => Date Time [yyyy-mm-dd hh:mm:ss]
     *
     */
    public function submitPlayerData(Request $request)
    {
        $json = array(
            'result' => 'Fail',
        );

        $name = $request->name ?? "";
        $email = $request->email;
        $contact = $request->contact ?? "";
        $age = $request->age ?? "";
        $dob = $request->dob ?? "";
        $gender = $request->gender ?? "";
        $source_identifier_code = $request->source_identifier_code ?? "";
        $created_at = $request->created_at ?? \Carbon\Carbon::now();

        $sourceIdentifierId = null;
        if ($source_identifier_code != "") {
            $sourceIdentifier = SourceIdentifier::where('code', $source_identifier_code)->first();
            $sourceIdentifierId = $sourceIdentifier->id;
        }

        // Check for duplication using email
        if (!is_null($email)) {
            $player = Player::where('email', $email)->first();

            if (!is_null($player)) {
                $json["result"] = "Duplicate";
                return response(json_encode($json), Response::HTTP_OK);
            }
        }

        // Check for duplication using contact
        if (!is_null($contact)) {
            $player = Player::where('contact', $contact)->first();

            if (!is_null($player)) {
                $json["result"] = "Duplicate";
                return response(json_encode($json), Response::HTTP_OK);
            }
        }


        $player = new Player;
        $player->name = $name;
        $player->email = $email;
        $player->contact = $contact;
        $player->age = $age;
        $player->dob = $dob;
        $player->gender = $gender;
        $player->source_identifier_id = $sourceIdentifierId;
        $player->created_at = $created_at;

        if ($player->save()) {
            $json["result"] = "Success";
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Check duplicate player registration API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Invalid Request
     * 2. New
     * 3. Duplicate
     *
     * Requires 1 parameters
     * 1. email
     *
     */
    public function checkDuplicatePlayer(Request $request)
    {
        $json = array(
            'result' => 'Invalid Request',
        );

        $email = $request->email;

        if (!is_null($email)) {
            $player = Player::where('email', $email)->first();

            if (is_null($player)) {
                $json["result"] = "New";
            } else {
                $json["result"] = "Duplicate";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Check duplicate redeem code API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Invalid Request
     * 2. New
     * 3. Duplicate
     *
     * Requires 1 parameters
     * 1. redeem_code
     *
     */
    public function checkDuplicateRedeemCode(Request $request)
    {
        $json = array(
            'result' => 'Invalid Request',
        );

        $redeem_code = $request->redeem_code;

        if (!is_null($redeem_code)) {
            $voucherDistribution = VoucherDistribution::where('redeem_code', $redeem_code)->first();

            if (is_null($voucherDistribution)) {
                $json["result"] = "New";
            } else {
                $json["result"] = "Duplicate";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Player's score data submission API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Success
     * 2. Fail
     *
     * Requires 3 parameters
     * 1. email
     * 2. score => Positive Integer
     * 3. result =>  ["win", "lose"]
     *
     */
    public function submitScoreData(Request $request)
    {
        $json = array(
            'result' => 'Fail',
        );

        $email = $request->email;
        $score = $request->score;
        $result = $request->result;

        $player = Player::where('email', $email)->first();

        if (!is_null($player)) {
            $player->scores()->create([
                'result' => $result,
                'score' => $score,
                'created_at' => $player->created_at,
            ]);

            $json["result"] = "Success";
        } else {
            $json["result"] = "Fail";
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Request for voucher API
     *
     * Requires 1 parameter
     * 1. email
     *
     * Response in json object from API
     * 1. result ["Success", "Fail"]
     * 2. voucher_code
     * 3. error_message
     *
     */
    public function requestVoucher(Request $request)
    {
        $json = array(
            'result' => 'Fail',
            'voucher_code' => '',
            'error_message' => '',
        );

        $email = $request->email;

        $in_stock_amount = 0;
        $random_number = 0;

        # Find the list of voucher with stock amount bigger than 0
        $voucher_stocks = VoucherStock::where('daily_remain', '>', 0)->get();

        # Find the player based on the email provided
        $player = Player::where('email', $email)->first();

        # Check if voucher still in stock
        if (!sizeof($voucher_stocks) < 1) {
            # Check if player exists
            if (!is_null($player)) {
                # Loop through voucher stock to get their remaining amount
                foreach ($voucher_stocks as $key => $voucher_stock) {
                    $in_stock_amount += $voucher_stock->daily_remain;
                }

                # Pick a voucher based on the probability calculated out of their number of available stock remain
                $random_number = rand(1, $in_stock_amount);

                # Ensure the generated random number is within the correct range
                if ($random_number > 0 && $random_number <= $in_stock_amount) {
                    # Loop through voucher stock to check what voucher has been selected
                    foreach ($voucher_stocks as $key => $voucher_stock) {
                        $random_number -= $voucher_stock->daily_remain;

                        # If the random number dropped below or equal zero, then the generated random number is within this prize's range
                        if ($random_number <= 0) {
                            $selected_voucher = Voucher::find($voucher_stock->voucher_id);

                            # Confirm the voucher stock voucher id has a host in voucher table
                            if (!is_null($selected_voucher)) {
                                # Decrement the voucher stock daily_remain by 1
                                $voucher_stock->daily_remain--;
                                # Increment the voucher stock requested_amount by 1
                                $voucher_stock->requested_amount++;

                                # Save the voucher stock changes
                                if ($voucher_stock->save()) {
                                    # Voucher code generation
                                    $voucher_code = self::generateRandomNumber();

                                    # Save record into Voucher Distributions
                                    $voucher_distribution = new VoucherDistribution();
                                    $voucher_distribution->voucher_id = $voucher_stock->voucher_id;
                                    $voucher_distribution->player_id = $player->id;
                                    $voucher_distribution->voucher_code = $voucher_code;

                                    if ($voucher_distribution->save()) {
                                        # Check for send voucher email configuration in .env
                                        $isSendVoucherEmail = env('TO_SEND_VOUCHER_EMAIL');

                                        # With sending voucher email
                                        if ($isSendVoucherEmail) {
                                            # Create an email jopb to send the voucher
                                            $send_voucher_email_job = new SendVoucherEmailJob();
                                            $send_voucher_email_job->player_id = $player->id;
                                            $send_voucher_email_job->voucher_id = $voucher_stock->voucher_id;

                                            if ($send_voucher_email_job->save()) {
                                                # Prepare the success json response message
                                                $json["result"] = "Success";
                                                $json["voucher_code"] =  $selected_voucher->code;
                                            } else {
                                                # If save to voucher distribution failed
                                                $json["result"] = "Fail";
                                                $json["error_message"] = "Fail to save into send voucher email job";
                                                $json["voucher_code"] =  "";
                                            }
                                        } else {
                                            # Without sending voucher email
                                            # Prepare the success json response message
                                            $json["result"] = "Success";
                                            $json["voucher_code"] =  $selected_voucher->code;
                                        }
                                    } else {
                                        # If save to voucher distribution failed
                                        $json["result"] = "Fail";
                                        $json["error_message"] = "Fail to save into voucher distribution";
                                        $json["voucher_code"] =  "";
                                    }
                                } else {
                                    $json["result"] = "Fail";
                                    $json["error_message"] = "Fail to save into voucher stock";
                                    $json["voucher_code"] =  "";
                                }
                                break;
                            }
                        }
                    }
                }
            } else {
                # If save to voucher distribution failed
                $json["result"] = "Fail";
                $json["error_message"] = "Invalid player email";
                $json["voucher_code"] =  "";
            }
        } else {
            # If save to voucher distribution failed
            $json["result"] = "Fail";
            $json["error_message"] = "All voucher out of stock";
            $json["voucher_code"] =  "";
        }

        # Return response
        return response(json_encode($json), Response::HTTP_OK);
    }


    /**
     * Voucher distribution data submission API
     *
     * Requires 2 parameters
     * 1. email
     * 2. voucher_code
     * 3. redeem_code
     *
     * Response in json object from API
     * 1. result ["Success", "Fail"]
     *
     */
    public function submitVoucherDistributeData(Request $request)
    {
        $json = array(
            'result' => 'Fail',
        );

        $email = $request->email;
        $voucher_code = $request->voucher_code;
        $redeem_code = $request->redeem_code;

        // Check for duplication using redeem_code
        if (!is_null($redeem_code)) {
            $voucherDistribution = VoucherDistribution::where('redeem_code', $redeem_code)->first();

            if (!is_null($voucherDistribution)) {
                $json["result"] = "Duplicate Redeem Code";
                $json["voucherDistributionId"] = $voucherDistribution->id;

                $fail_request = new VoucherDistributionsFailRequest();
                $fail_request->redeem_code = $redeem_code;
                $fail_request->result = $json["result"];
                $fail_request->save();

                return response(json_encode($json), Response::HTTP_OK);
                
            }
        }
        else{
            $fail_request = new VoucherDistributionFailsRequest();
            $fail_request->redeem_code = $redeem_code;
            $fail_request->result = $json["result"];
            $fail_request->save();
        }

        $player = Player::where('email', $email)->first();
        // $voucher = Voucher::where('code', $voucher_code)->first();

        $voucher_distribution = new VoucherDistribution();

        // $voucher_distribution->voucher_id = $voucher->id;

        // only have one voucher here
        $voucher_distribution->voucher_id = 1;
        $voucher_distribution->player_id = $player->id;
        $voucher_distribution->redeem_code = $redeem_code;

        if ($request->created_at != null) {
            $voucher_distribution->created_at = $request->created_at;
        } else {
            $voucher_distribution->created_at = \Carbon\Carbon::now();
        }

        if ($voucher_distribution->save()) {
            $json["result"] = "Success";
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Resets stock management every 12AM of the day
     */
    public function dailyStockReset()
    {
        if (\Carbon\Carbon::now() <= strtotime("12:59am") && \Carbon\Carbon::now() >= strtotime("12:01am")) {
            # Get the list of voucher stock
            $voucher_stocks = VoucherStock::all();

            foreach ($voucher_stocks as $key => $voucher_stock) {
                # Set the daily remain stock amount to default quota amount
                $voucher_stock->daily_remain = $voucher_stock->daily_quota;
                # Save the changes
                $voucher_stock->save();
            }
        }
    }

    /**
     * Generate txt file of registered emails API
     */
    public function generatePlayerEmailList()
    {
        $players = Player::all();

        foreach ($players as $player) {
            Storage::append('temp_registered_emails.txt', $player->email);
        }

        // Delete the old final list file
        if (Storage::exists('registered_emails.txt'))
            Storage::delete('registered_emails.txt');

        // Rename the temp file to final list file
        Storage::move('temp_registered_emails.txt', 'registered_emails.txt');

        // Delete the old temp list file
        if (Storage::exists('temp_registered_emails.txt'))
            Storage::delete('temp_registered_emails.txt');
    }

    /**
     * Generate public accessible url for txt file of registered emails
     */
    public function downloadPlayerEmailList()
    {
        return Storage::download('registered_emails.txt');
    }

    /**
     * Generate txt file of registered emails API
     */
    public function generateUsedRedeemCodeList()
    {
        $voucher_distributions = VoucherDistribution::all();

        foreach ($voucher_distributions as $voucher_distribution) {
            Storage::append('temp_used_redeem_codes.txt', $voucher_distribution->redeem_code);
        }

        // Delete the old final list file
        if (Storage::exists('used_redeem_codes.txt'))
            Storage::delete('used_redeem_codes.txt');

        // Rename the temp file to final list file
        Storage::move('temp_used_redeem_codes.txt', 'used_redeem_codes.txt');

        // Delete the old temp list file
        if (Storage::exists('temp_used_redeem_codes.txt'))
            Storage::delete('temp_used_redeem_codes.txt');
    }

    /**
     * Generate public accessible url for txt file of registered emails
     */
    public function downloadUsedRedeemCodeList()
    {
        return Storage::download('used_redeem_codes.txt');
    }

    /**
     * Send Voucher Email API
     */
    public function sendVoucherEmail()
    {
        $send_voucher_email_jobs = SendVoucherEmailJob::where('isEmailSent', 0)->orderBy('id', 'asc')->get();

        foreach ($send_voucher_email_jobs as $send_voucher_email_job) {
            // Get the player's details
            $player = Player::find($send_voucher_email_job->player_id);
            $voucher = Voucher::find($send_voucher_email_job->voucher_id);
            $voucher_code = VoucherDistribution::where('player_id', $send_voucher_email_job->player_id)
                ->where('voucher_id', $send_voucher_email_job->voucher_id)->first();

            // Mail the user voucher
            // Mail details
            $details = [
                'title' => 'Unicom Interactive Digital Voucher Email',
                'image' => $voucher->image,
                'voucher_code' => $voucher_code->voucher_code,
                'player' => $player->player
            ];

            // Send mail
            \Mail::to($player->email)->send(new \App\Mail\SendVoucherEmail($details));

            $send_voucher_email_job->isEmailSent = true;
            $send_voucher_email_job->emailSentAt = \Carbon\Carbon::now();
            $send_voucher_email_job->save();
        }
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Get the list of source identifiers
     *
     * return a list of all source identifiers in the database
     *
     */
    public function getSourceIdentifierList()
    {
        $sourceIdentifiers = DB::table('source_identifiers')
            ->join('events', 'source_identifiers.event_id', '=', 'events.id')
            ->join('locations', 'source_identifiers.location_id', '=', 'locations.id')
            ->select('events.event_code', 'locations.location', 'source_identifiers.code', 'source_identifiers.description')
            ->get();

        return response(json_encode($sourceIdentifiers), Response::HTTP_OK);
    }

    /**
     * [Genting Project]
     * Random Number Generator
     */
    public function generateRandomNumber()
    {
        $number = mt_rand(100000, 999999);
        $voucher_distributions = VoucherDistribution::where('voucher_code', $number)->get();

        if (sizeof($voucher_distributions) > 0) {
            self::generateRandomNumber();
        } else if (sizeof($voucher_distributions) == 0) {
            return $number;
        }
    }
}
