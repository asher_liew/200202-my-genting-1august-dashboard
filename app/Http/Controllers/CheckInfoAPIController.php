<?php

namespace App\Http\Controllers;

use App\Event;
use App\Player;
use App\Score;
use App\SendVoucherEmailJob;
use App\SourceIdentifier;
use App\Voucher;
use App\VoucherDistribution;
use App\VoucherStock;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class CheckInfoAPIController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Check duplicate player registration API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Invalid Request
     * 2. New
     * 3. Duplicate
     *
     * Requires 1 parameters
     * 1. email
     * 2. contact
     *
     */
    public function checkDuplicatePlayer(Request $request)
    {
        $json = array(
            'result' => 'Invalid Request',
            'reason' => 'NA'
        );

        $email = $request->email ?? "";
        $contact = $request->contact ?? "";

        $duplicatedEmail = false;
        $duplicatedContact = false;

        if (!is_null($email)) {

            // Scan through players table for similar email
            $player_based_email = Player::where('email', $email)->first();

            if (!is_null($player_based_email)) {
                $duplicatedEmail = true;
            }
        }

        if (!is_null($contact)) {

            // Scan through players table for similar contact
            $player_based_contact = Player::where('contact', $contact)->first();

            if (!is_null($player_based_contact)) {
                $duplicatedContact = true;
            }
        }

        // Set the result based on whether either email or contact or both are duplicated or not
        if ($duplicatedEmail) {
            $json["result"] = "Fail";
            $json["reason"] = "Duplicate Email.";
        } else if ($duplicatedContact) {
            $json["result"] = "Fail";
            $json["reason"] = "Duplicate Contact.";
        } else {
            $json["result"] = "New";
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Check duplicate redeem code API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Invalid Request
     * 2. New
     * 3. Duplicate
     *
     * Requires 1 parameters
     * 1. redeem_code
     *
     */
    public function checkDuplicateRedeemCode(Request $request)
    {
        $json = array(
            'result' => 'Invalid Request',
            'reason' => 'NA'
        );

        $redeem_code = $request->redeem_code;

        if (!is_null($redeem_code)) {
            $voucherDistribution = VoucherDistribution::where('redeem_code', $redeem_code)->first();

            if (is_null($voucherDistribution)) {
                $json["result"] = "New";
            } else {
                $json["result"] = "Duplicate";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }
}
