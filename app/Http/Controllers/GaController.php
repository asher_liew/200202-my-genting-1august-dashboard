<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google_Client;

define("VIEW_ID", "204606995");

class GaController extends Controller
{
    public function getUserJourney() {
        $analytics = self::initializeAnalytics();
        $response = self::getUserJourneyReport($analytics);
        $resultList = self::getUserJourneyResults($response);

        return response()->json($resultList);
    }

    public function getVisitCount() {
        $analytics = self::initializeAnalytics();

        // Get the overall report
        $response = self::getVisitReport($analytics);
        $visitList = self::getVisitResults($response);

        // Get the report for current day (To ne shown under the number of gameplays graph)
        $response = self::getVisitReportToday($analytics);
        $visitListToday = self::getVisitResults($response);

        $visitList = array_merge($visitList, $visitListToday);

        return response()->json($visitList);
    }

    public function getVisitCountDate(Request $request) {
        $date = $request->date;

        $analytics = self::initializeAnalytics();
        $response = self::getVisitReportDate($analytics, $date);
        $visitList = self::getVisitResults($response);

        return response()->json($visitList);
    }

    /**
     * Initializes an Analytics Reporting API V4 service object.
     *
     * @return An authorized Analytics Reporting API V4 service object.
     */
    public function initializeAnalytics() {

        // Use the developers console and download your service account
        // credentials in JSON format. Place them in this directory or
        // change the key file location if necessary.
        $KEY_FILE_LOCATION = '../service-account-credentials.json';

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->getCache()->clear();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $analytics = new \Google_Service_AnalyticsReporting($client);

        return $analytics;
    }


    /**
     * Queries the Analytics Reporting API V4.
     *
     * @param service An authorized Analytics Reporting API V4 service object.
     * @return The Analytics Reporting API V4 response.
     */
    public function getUserJourneyReport($analytics) {
        // Create the DateRange object.
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("2019-01-01");
        $dateRange->setEndDate("today");

        // Create the Metrics objects.
        $pageviews = new \Google_Service_AnalyticsReporting_Metric();
        $pageviews->setExpression("ga:pageviews");
        $pageviews->setAlias("pageviews");

        //Create the Dimensions object.
        $pageTitle = new \Google_Service_AnalyticsReporting_Dimension();
        $pageTitle->setName("ga:pageTitle");

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId(VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($pageviews));
        $request->setDimensions(array($pageTitle));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request) );
        return $analytics->reports->batchGet( $body );
    }

    public function getVisitReport($analytics) {
        // Create the DateRange object.
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("2019-01-01");
        $dateRange->setEndDate("today");

        // Create the Metrics objects.
        $pageviews = new \Google_Service_AnalyticsReporting_Metric();
        $pageviews->setExpression("ga:pageviews");
        $pageviews->setAlias("pageviews");

        $uniquePageviews = new \Google_Service_AnalyticsReporting_Metric();
        $uniquePageviews->setExpression("ga:uniquePageviews");
        $uniquePageviews->setAlias("uniquePageviews");

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId(VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($pageviews, $uniquePageviews));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request) );
        return $analytics->reports->batchGet( $body );
    }

    public function getVisitReportToday($analytics) {
        // Create the DateRange object.
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("today");
        $dateRange->setEndDate("today");

        // Create the Metrics objects.
        $pageviews = new \Google_Service_AnalyticsReporting_Metric();
        $pageviews->setExpression("ga:pageviews");
        $pageviews->setAlias("pageviews");

        $uniquePageviews = new \Google_Service_AnalyticsReporting_Metric();
        $uniquePageviews->setExpression("ga:uniquePageviews");
        $uniquePageviews->setAlias("uniquePageviews");

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId(VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($pageviews, $uniquePageviews));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request) );
        return $analytics->reports->batchGet( $body );
    }

    public function getVisitReportDate($analytics, $date) {
        // Create the DateRange object.
        $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($date);
        $dateRange->setEndDate($date);

        // Create the Metrics objects.
        $pageviews = new \Google_Service_AnalyticsReporting_Metric();
        $pageviews->setExpression("ga:pageviews");
        $pageviews->setAlias("pageviews");

        $uniquePageviews = new \Google_Service_AnalyticsReporting_Metric();
        $uniquePageviews->setExpression("ga:uniquePageviews");
        $uniquePageviews->setAlias("uniquePageviews");

        // Create the ReportRequest object.
        $request = new \Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId(VIEW_ID);
        $request->setDateRanges($dateRange);
        $request->setMetrics(array($pageviews, $uniquePageviews));

        $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $request) );
        return $analytics->reports->batchGet( $body );
    }

    /**
     * Parses and prints the Analytics Reporting API V4 response.
     *
     * @param An Analytics Reporting API V4 response.
     */
    public function getUserJourneyResults($reports) {
        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            $resultList = array();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $result = new Results;

                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                    $result->title = $dimensions[$i];
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        $result->pageviews = $values[$k];
                    }
                }

                array_push($resultList, $result);
            }
        }

        return $resultList;
    }

    public function getVisitResults($reports) {
        $visitList = array();

        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[$reportIndex];
            $rows = $report->getData()->getRows();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $metrics = $row->getMetrics();

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        array_push($visitList, $values[$k]);
                    }
                }
            }
        }

        return $visitList;
    }
}

class Results {
    var $title;
    var $pageviews;
}
