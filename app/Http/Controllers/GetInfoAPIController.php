<?php

namespace App\Http\Controllers;

use App\Event;
use App\Player;
use App\Score;
use App\SendVoucherEmailJob;
use App\SourceIdentifier;
use App\Voucher;
use App\VoucherDistribution;
use App\VoucherStock;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class GetInfoAPIController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Return number of gameplay of the day based on the member ID
     *
     * 1 possible result response in json object [result] from this get number of gameplay of the day api:
     * 1. number of gameplay of the day [integer]
     *
     * Requires 1 parameters
     * 1. email
     *
     */
    public function getTodayNumGameplay(Request $request)
    {
        $json = array(
            'result' => '0',
        );

        $email = $request->email;

        if (!is_null($email)) {
            $player = Player::where('email', $email)->first();

            if (!is_null($player)) {
                $numOfGameplayOfTheDay = Score::where('player_id', $player->id)->whereDate('created_at', date("Y-m-d"))->count();
                $json["result"] = (string) $numOfGameplayOfTheDay;
            } else {
                // No player found
                $json["result"] = "0";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }


    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Return cumulative score based on the member ID
     *
     * 1 possible result response in json object [result] from this get cumulative gameplay score api:
     * 1. cumulative gameplay score [integer]
     *
     * Requires 1 parameters
     * 1. email
     *
     */
    public function getCumulativeScore(Request $request)
    {
        $json = array(
            'result' => '0',
        );

        $email = $request->email;
        $cumulativeScore = 0;

        if (!is_null($email)) {
            $player = Player::where('email', $email)->first();

            if (!is_null($player)) {
                // Get a sum from rows of scores from table
                $cumulativeScore = Score::where('player_id', $player->id)->sum('score');
                $json["result"] = (string) $cumulativeScore;
            } else {
                // No player found
                $json["result"] = "0";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }
}
