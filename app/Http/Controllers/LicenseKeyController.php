<?php

namespace App\Http\Controllers;

use App\LicenseKey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class LicenseKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Activate License Key API
     * 4 possible results from this activate license api:
     * 1. Invalid License Key
     * 2. Expired License Key
     * 3. Used License Key
     * 4. Valid License Key
     */
    public function activateLicenseKey(Request $request)
    {
        $license_key = $request->license_key;
        $mic = $request->mic;
        $toActivate = false;

        $json = array(
            'result' => 'Invalid License Key',
            'license_key_expiry_date' => '',
        );

        $license = LicenseKey::where('license_key', $license_key)->first();

        if ($license != null) {
            // Found the license key
            // It is not activated
            // It is not expired
            if ($license->isActivated != true && $license->license_key_expiry_date > \Carbon\Carbon::now()) {
                $json["result"] = "Valid License Key";
                $json["license_key_expiry_date"] = $license->license_key_expiry_date;
                $toActivate = true;
            } else if ($license->isActivated == true && $license->license_key_expiry_date > \Carbon\Carbon::now()) {
                $json["result"] = "Used License Key";
                $json["license_key_expiry_date"] = $license->license_key_expiry_date;
            } else if ($license->license_key_expiry_date < \Carbon\Carbon::now()) {
                $json["result"] = "Expired License Key";
            } else {
                $json["result"] = "Invalid License Key";
            }
        } else {
            $json["result"] = "Invalid License Key";
        }

        // If validatity is true, activate it
        if ($toActivate)
        {
            $license->machine_identity_code = $mic;
            $license->license_key_activate_at = \Carbon\Carbon::now()->format('Y-m-d');
            $license->isActivated = true;
            $license->save();
        }

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Validate License Key API
     * 3 possible results from this activate license api:
     * 1. Invalid Validation
     * 2. Expired License Key
     * 3. Valid Validation
     */
    public function validateLicenseKey(Request $request)
    {
        $license_key = $request->license_key;
        $mic = $request->mic;
        $updateLastValidate = false;

        $json = array(
            'result' => 'Invalid Validation',
        );

        $license = LicenseKey::where('license_key', $license_key, 'machine_identity_code', $mic)->first();

        if ($license != null) {
            // Found the license key
            // It is not activated
            // It is not expired
            if ($license->isActivated == true && $license->license_key_expiry_date > \Carbon\Carbon::now()) {
                $json["result"] = "Valid Validation";
                $updateLastValidate = true;
            } else if ($license->license_key_expiry_date < \Carbon\Carbon::now()) {
                $json["result"] = "Expired License Key";
            } else {
                $json["result"] = "Invalid Validation";
            }
        } else {
            $json["result"] = "Invalid Validation";
        }

        // If validatity is true, activate it
        if ($updateLastValidate)
        {
            $license->last_validate_at = \Carbon\Carbon::now();
            $license->save();
        }

        Log::debug('License key validate result: ' . $json["result"]);

        return response(json_encode($json), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
