<?php

namespace App\Http\Controllers;

use App\Player;
use App\Score;
use App\Share;
use App\VoucherDistribution;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class MainController extends Controller
{
    /**
     * Protect the productsview page from unauthenticated using 2FA
     */
    public function __construct()
    {
        // $this->middleware('2fa_auth');
        $this->middleware('auth');
    }

    /**
     * Get the saved event ID from the cookie
     */
    public function getEventId() {
        $eventId = 1;
        if (isset($_COOKIE["eventID"])) {
            if ($_COOKIE["eventID"] != "") {
                $eventId = $_COOKIE["eventID"];
            }
        }

        return $eventId;
    }

    /**
     * Get the saved source identifier codes from the cookie
     */
    public function getSourceIdentifierCode() {
        $eventId = self::getEventId();

        $sourceIdentifierCode = DB::table('source_identifiers')
                                ->join('events', 'source_identifiers.event_id', '=', 'events.id')
                                ->join('locations', 'source_identifiers.location_id', '=', 'locations.id')
                                ->select('source_identifiers.code')
                                ->where('source_identifiers.event_id', $eventId)
                                ->get();

        $sourceIdentifierCode = json_decode(json_encode($sourceIdentifierCode), true);

        if (isset($_COOKIE["sourceIdentifierCode"])) {
            if ($_COOKIE["sourceIdentifierCode"] != "") {
                $sourceIdentifierCode = array();
                $sourceIdentifierCode = explode(',', $_COOKIE["sourceIdentifierCode"]);
            }
        }

        return $sourceIdentifierCode;
    }

    /**
     * Display various information to be shown in the homepage.
     *
     * @return view containing various data as parameters
     */
    public function index()
    {
        $eventId = self::getEventId();
        $sourceIdentifierCode = self::getSourceIdentifierCode();

        return view('dashboard.home')
            ->with('playerCount', self::getPlayerCount($eventId, $sourceIdentifierCode))
            ->with('shareInfo', self::getShareInfo($eventId, $sourceIdentifierCode))
            ->with('totalDistVoucher', self::getDistributedVoucherCount($eventId, $sourceIdentifierCode))
            ->with('totalClaimedVoucher', self::getClaimedVoucherCount($eventId, $sourceIdentifierCode))
            ->with('i', 0)
            ->with('playersYearAndWeek', self::getPlayersYearsAndWeeks($eventId, $sourceIdentifierCode));
    }

    /**
     * Get the total registered players count
     *z
     * @return number of rows in the players table
     */
    public function getPlayerCount($eventId, $sourceIdentifierCode)
    {
        $players = DB::table('players')
                ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
                ->where('source_identifiers.event_id', $eventId)
                ->whereIn('source_identifiers.code', $sourceIdentifierCode)
                ->get();

        return sizeof($players);
    }

    /**
     * Get the count of total shares, Facebook, Twitter and WhatsApp
     *
     * @return array of object
     */
    public function getShareInfo($eventId, $sourceIdentifierCode)
    {
        $shares = DB::table('shares')
                    ->join('scores', 'shares.score_id', '=', 'scores.id')
                    ->join('players', 'scores.player_id', '=', 'players.id')
                    ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
                    ->where('source_identifiers.event_id', $eventId)
                    ->whereIn('source_identifiers.code', $sourceIdentifierCode)
                    ->get();

        $totalFB = 0;
        $totalTW = 0;
        $totalWA = 0;

        foreach ($shares as $share) {
            switch ($share->platform) {
                case "Facebook":
                    $totalFB++;
                    break;

                case "Twitter":
                    $totalTW++;
                    break;

                case "WhatsApp":
                    $totalWA++;
                    break;
            }
        }

        $shareInfo = array(
            'totalShare' => sizeof($shares),
            'totalShareFB' => $totalFB,
            'totalShareTW' => $totalTW,
            'totalShareWA' => $totalWA
        );

        return $shareInfo;
    }

    /**
     * Get the total distributed voucher count
     *
     * @return number of rows in the voucher_distributions table
     */
    public function getDistributedVoucherCount($eventId, $sourceIdentifierCode)
    {
        $vouchers = DB::table('voucher_distributions')
            ->join('players', 'voucher_distributions.player_id', '=', 'players.id')
            ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->where('source_identifiers.event_id', $eventId)
            ->whereIn('source_identifiers.code', $sourceIdentifierCode)
            ->get();

        return sizeof($vouchers);
    }

    /**
     * Get the total redeemed voucher count
     *
     * @return number of rows in the voucher_distributions table where redeem_time columm is not null
     */
    public function getClaimedVoucherCount($eventId, $sourceIdentifierCode)
    {
        $vouchers = VoucherDistribution::whereNotNull('redeem_location_id')
            ->join('players', 'voucher_distributions.player_id', '=', 'players.id')
            ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->where('source_identifiers.event_id', $eventId)
            ->whereIn('source_identifiers.code', $sourceIdentifierCode)
            ->whereNotNull('redeem_time')
            ->get();

        return sizeof($vouchers);
    }

    /**
     * Get a list of years and weeks from the players data to be shown in the week number dropdown
     *
     * @return list of years and weeks
     */
    public function getPlayersYearsAndWeeks($eventId, $sourceIdentifierCode) {
        $players = DB::table('players')
                    ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
                    ->select(DB::raw('YEAR(players.created_at) as year'), DB::raw('WEEK(players.created_at, 1) as week'))
                    // ->where('source_identifiers.event_id', $eventId)
                    // ->whereIn('source_identifiers.code', $sourceIdentifierCode)
                    ->groupBy(DB::raw('WEEK(players.created_at)'))
                    ->orderBy('players.created_at')
                    ->get();

        return $players;
    }
}
