<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class OverviewController extends Controller
{
    /**
     * Get the saved event ID from the cookie
     */
    public function getEventId()
    {
        $eventId = 1;
        if (isset($_COOKIE["eventID"])) {
            if ($_COOKIE["eventID"] != "") {
                $eventId = $_COOKIE["eventID"];
            }
        }

        return $eventId;
    }

    /**
     * Get the saved source identifier codes from the cookie
     */
    public function getSourceIdentifierCode() {
        $eventId = self::getEventId();

        $sourceIdentifierCode = DB::table('source_identifiers')
                                ->join('events', 'source_identifiers.event_id', '=', 'events.id')
                                ->join('locations', 'source_identifiers.location_id', '=', 'locations.id')
                                ->select('source_identifiers.code')
                                ->where('source_identifiers.event_id', $eventId)
                                ->get();

        $sourceIdentifierCode = json_decode(json_encode($sourceIdentifierCode), true);

        if (isset($_COOKIE["sourceIdentifierCode"])) {
            if ($_COOKIE["sourceIdentifierCode"] != "") {
                $sourceIdentifierCode = array();
                $sourceIdentifierCode = explode(',', $_COOKIE["sourceIdentifierCode"]);
            }
        }

        return $sourceIdentifierCode;
    }

    public function getDailyOverview(Request $request)
    {
        $eventId = self::getEventId();
        $sourceIdentifierCode = self::getSourceIdentifierCode();

        $tableName = $request->tableName;

        $query = DB::table($tableName);

        if ($tableName != "players") {
            $query->join('players', 'voucher_distributions.player_id', '=', 'players.id');
        }

        $query->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->select(DB::raw('DATE(' .$tableName.'.created_at) as date'), DB::raw('COUNT(DATE(' .$tableName.'.created_at)) as count'))
            ->where('source_identifiers.event_id', $eventId)
            ->whereIn('source_identifiers.code', $sourceIdentifierCode)
            ->groupBy(DB::raw('DATE(' .$tableName.'.created_at)'));

        $dailyOverviewList = $query->get();

        for ($i = 0; $i < sizeof($dailyOverviewList); $i++) {
            $dailyOverviewList[$i]->date = date("d/m/y", strtotime($dailyOverviewList[$i]->date));
        }

        return response(json_encode($dailyOverviewList), Response::HTTP_OK);
    }

    public function getWeeklyOverview(Request $request)
    {
        $eventId = self::getEventId();
        $sourceIdentifierCode = self::getSourceIdentifierCode();

        $tableName = $request->tableName;

        $query = DB::table($tableName);

        if ($tableName != "players") {
            $query->join('players', 'voucher_distributions.player_id', '=', 'players.id');
        }

        $query->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->select(DB::raw('WEEK(' .$tableName.'.created_at) as week'), DB::raw('COUNT(WEEK(' .$tableName.'.created_at)) as count'))
            ->where('source_identifiers.event_id', $eventId)
            ->whereIn('source_identifiers.code', $sourceIdentifierCode)
            ->groupBy(DB::raw('WEEK(' .$tableName.'.created_at)'));

        $weeklyOverviewList = $query->get();

        return response(json_encode($weeklyOverviewList), Response::HTTP_OK);
    }

    public function getBranchDailyOverview()
    {
        $eventId = self::getEventId();

        // Get the branch list
        $branchList = Location::select('location')
            ->where('event_id', $eventId)
            ->orderBy('id')
            ->get();

        $branchDailyOverviewList = DB::table('players')
            ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->join('locations', 'source_identifiers.location_id', '=', 'locations.id')
            ->select('locations.location', DB::raw('COUNT(DATE(players.created_at)) as count'), DB::raw('DATE_FORMAT(players.created_at, "%d/%m/%y") as date'))
            ->where('source_identifiers.event_id', $eventId)
            ->groupBy('locations.location', DB::raw('DATE(players.created_at)'))
            ->orderBy('locations.id')
            ->orderBy(DB::raw('DATE(players.created_at)'))
            ->get();

        $resultSet = array(
            'branchList' => $branchList,
            'branchDailyOverviewList' => $branchDailyOverviewList
        );

        return response(json_encode($resultSet), Response::HTTP_OK);
    }

    /**
     * Get the number of registration for current date based on hour
     *
     * @return list of number of hourly gameplays
     */
    public function getRegistrationHourCount(Request $request)
    {
        $eventId = self::getEventId();
        $sourceIdentifierCode = self::getSourceIdentifierCode();

        $date = $request->date;
        $date2 = date("Y-m-d", strtotime("+1 day", strtotime($date)));

        $players = DB::table('voucher_distributions')
            ->join('players', 'players.id', '=', 'voucher_distributions.player_id')
            ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->select(DB::raw('COUNT(hour(voucher_distributions.created_at)) as count'), DB::raw('hour(voucher_distributions.created_at) as hour'))
            ->where('voucher_distributions.created_at', '>=', $date)
            ->where('voucher_distributions.created_at', '<', $date2)
            ->where('source_identifiers.event_id', $eventId)
            ->whereIn('source_identifiers.code', $sourceIdentifierCode)
            ->groupBy(DB::raw('hour(voucher_distributions.created_at)'))
            ->get();

        $registrationHourCountList = array();
        // Initialize registrationHourCountList
        for ($i = 0; $i < 24; $i++) {
            $registrationHourCountList[$i] = 0;
        }

        foreach ($players as $player) {
            $hour = $player->hour;

            $registrationHourCountList[$hour] = $player->count;
        }

        return response(json_encode($registrationHourCountList), Response::HTTP_OK);
    }

    /**
     * Get the
     */
}
