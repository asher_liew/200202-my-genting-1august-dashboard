<?php

namespace App\Http\Controllers;

use App\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response as FacadeResponse;

class ScoreController extends Controller
{
    /**
     * Protect the productsview page from unauthenticated visitor
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the saved event ID from the cookie
     */
    public function getEventId()
    {
        $eventId = 1;
        if (isset($_COOKIE["eventID"])) {
            if ($_COOKIE["eventID"] != "") {
                $eventId = $_COOKIE["eventID"];
            }
        }

        return $eventId;
    }

    /**
     * Get the saved source identifier codes from the cookie
     */
    public function getSourceIdentifierCode() {
        $eventId = self::getEventId();

        $sourceIdentifierCode = DB::table('source_identifiers')
                                ->join('events', 'source_identifiers.event_id', '=', 'events.id')
                                ->join('locations', 'source_identifiers.location_id', '=', 'locations.id')
                                ->select('source_identifiers.code')
                                ->where('source_identifiers.event_id', $eventId)
                                ->get();

        $sourceIdentifierCode = json_decode(json_encode($sourceIdentifierCode), true);

        if (isset($_COOKIE["sourceIdentifierCode"])) {
            if ($_COOKIE["sourceIdentifierCode"] != "") {
                $sourceIdentifierCode = array();
                $sourceIdentifierCode = explode(',', $_COOKIE["sourceIdentifierCode"]);
            }
        }

        return $sourceIdentifierCode;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventId = self::getEventId();
        $sourceIdentifierCode = self::getSourceIdentifierCode();

        $scores = DB::table('scores')
                    ->join('players', 'scores.player_id', '=', 'players.id')
                    ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
                    ->select('scores.*', 'players.name')
                    ->where('source_identifiers.event_id', $eventId)
                    ->whereIn('source_identifiers.code', $sourceIdentifierCode)
                    ->get();

        return view('dashboard.scores', compact('scores'))
            ->with('i', 0);
    }

    public function downloadCSV()
    {
        $eventId = self::getEventId();
        $sourceIdentifierCode = self::getSourceIdentifierCode();

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $filename = "scores_info_" . date("Y-m-d_His");

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=' . $filename . '.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $list = DB::table('scores')
            ->join('players', 'scores.player_id', '=', 'players.id')
            ->join('source_identifiers', 'players.source_identifier_id', '=', 'source_identifiers.id')
            ->select('players.name as Player Name', 'scores.score as Score', 'scores.result as Result', 'scores.created_at as Played At')
            ->where('source_identifiers.event_id', $eventId)
            ->whereIn('source_identifiers.code', $sourceIdentifierCode)
            ->get()->toArray();

        $list = json_decode(json_encode($list), true);

        if (sizeof($list) > 0) {
            # add headers for each column in the CSV download
            array_unshift($list, array_keys($list[0]));

            $callback = function () use ($list) {
                $FH = fopen('php://output', 'w');
                foreach ($list as $row) {
                    fputcsv($FH, $row);
                }
                fclose($FH);
            };

            return FacadeResponse::stream($callback, 200, $headers);
        } else {
            return self::index();
        }
    }
}
