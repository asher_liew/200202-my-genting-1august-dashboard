<?php

namespace App\Http\Controllers;

use App\Event;
use App\Player;
use App\Score;
use App\SendVoucherEmailJob;
use App\SourceIdentifier;
use App\Voucher;
use App\VoucherDistribution;
use App\VoucherStock;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class SubmitInfoAPIController extends Controller
{

    /*****************************************************
     *****************  External APIs  *******************
     ****************************************************/


    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Player data submission API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Success
     * 2. Fail
     *
     * Requires 8 parameters
     * 1. name
     * 2. email
     * 3. contact => String with country code in front of every phone number
     * 4. age => Positive Integer
     * 5. dob => Date [yyyy-mm-dd]
     * 6. gender => ["0", "male", "female", "other"]
     * 7. source_identifier_code
     * 8. created_at => Date Time [yyyy-mm-dd hh:mm:ss]
     *
     */
    public function submitPlayerData(Request $request)
    {
        $dump = false;

        $json = array(
            'result' => 'Fail',
            'reason' => 'NA'
        );

        $name = $request->name ?? "";
        $email = $request->email;
        $contact = $request->contact ?? "";
        $age = $request->age ?? "";
        $dob = $request->dob ?? "";
        $gender = $request->gender ?? "";
        $source_identifier_code = $request->source_identifier_code ?? "";
        $created_at = $request->created_at ?? \Carbon\Carbon::now();

        // Query source identifier ID based on source_identifier_code submitted
        if ($source_identifier_code != "") {
            $sourceIdentifier = SourceIdentifier::where('code', $source_identifier_code)->first();

            if ($sourceIdentifier != null) {

                $sourceIdentifierId = $sourceIdentifier->id;

                // Check for duplication using email
                if (!is_null($email)) {
                    // Look for the player based on email
                    $player = Player::where('email', $email)->first();

                    // If the player is found to be registered before
                    if (!is_null($player)) {
                        $dump = true;
                        $json["result"] = "Fail";
                        $json["reason"] = "Duplicate Email.";
                    }
                }
                // If email is not submitted
                else {
                    $dump = true;
                    $json["result"] = "Fail";
                    $json["reason"] = "Email not submitted.";
                }

                // Check for duplication using contact
                if (!is_null($contact)) {
                    $player = Player::where('contact', $contact)->first();

                    // If the player is found to be registered before
                    if (!is_null($player)) {
                        $dump = true;
                        $json["result"] = "Fail";
                        $json["reason"] = "Duplicate Contact.";
                    }
                }
                // If contact is not submitted
                else {
                    $dump = true;
                    $json["result"] = "Fail";
                    $json["reason"] = "Contact not submitted.";
                }
            }
            // If source identifier ID is not found
            else {
                $dump = true;
                $json["result"] = "Fail";
                $json["reason"] = "Source Identifier not found.";
            }
        }
        // If source identifier ID is not submitted
        else {
            $dump = true;
            $json["result"] = "Fail";
            $json["reason"] = "Source Identifier not submitted.";
        }


        if ($dump) {
            // Save into dump table
            DB::table('players_data_dump')->insert(
                [
                    'name' => $name,
                    'email' => $email,
                    'contact' => $contact,
                    'age' => $age,
                    'dob' => $dob,
                    'gender' => $gender,
                    'source_identifier_code' => $source_identifier_code,
                    'created_at' => $created_at
                ]
            );

            $json["result"] = "Technical Error: Saved into Dump table";

        } else {
            $player = new Player;
            $player->name = $name;
            $player->email = $email;
            $player->contact = $contact;
            $player->age = $age;
            $player->dob = $dob;
            $player->gender = $gender;
            $player->source_identifier_id = $sourceIdentifierId;
            $player->created_at = $created_at;

            if ($player->save()) {
                $json["result"] = "Success";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }


    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * Player's score data submission API
     *
     * 2 possible results response in json object [result] from this player data submission api:
     * 1. Success
     * 2. Fail
     *
     * Requires 3 parameters
     * 1. email
     * 2. score => Positive Integer
     * 3. result =>  ["win", "lose"]
     *
     */
    public function submitScoreData(Request $request)
    {
        $dump = false;

        $json = array(
            'result' => 'Fail',
            'reason' => 'NA'
        );

        $email = $request->email;
        $score = $request->score;
        $result = $request->result;

        $player = Player::where('email', $email)->first();

        if (!is_null($player)) {
            $player->scores()->create([
                'score' => $score,
                'result' => $result,
                'created_at' => $player->created_at,
            ]);

            $json["result"] = "Success";
        }
        // The player ID cannot be found
        else {
            // Save into dump table
            DB::table('scores_data_dump')->insert(
                [
                    'email' => $email,
                    'score' => $score,
                    'result' => $result,
                    'created_at' => \Carbon\Carbon::now()
                ]
            );
            $json["result"] = "Fail";
            $json["reason"] = "Error: Player data not found. Saved into Dump table.";
        }

        return response(json_encode($json), Response::HTTP_OK);
    }


    /**
     * Voucher distribution data submission API
     *
     * Requires 2 parameters
     * 1. email
     * 2. voucher_code
     * 3. redeem_code
     *
     * Response in json object from API
     * 1. result ["Success", "Fail"]
     *
     */
    public function submitVoucherDistributeData(Request $request)
    {
        $dump = false;

        $json = array(
            'result' => 'Fail',
            'reason' => 'NA'
        );

        $email = $request->email;
        $voucher_code = $request->voucher_code;
        $redeem_code = $request->redeem_code;
        $created_at = $request->created_at ?? \Carbon\Carbon::now();

        // Check for duplication using redeem_code
        if (!is_null($redeem_code)) {
            $voucherDistribution = VoucherDistribution::where('redeem_code', $redeem_code)->first();

            // If duplicate redeem code is found
            if (!is_null($voucherDistribution)) {
                $dump = true;
                $json["result"] = "Fail";
                $json["reason"] = "Error: Duplicate Redeem Cod. Saved into Dump table.";
            }
        }
        // No redeem code is submitted
        else {
            $dump = true;
            $json["result"] = "Fail";
            $json["reason"] = "Error: Redeem code not submitted. Saved into Dump table.";
        }

        // Query for player data
        $player = Player::where('email', $email)->first();

        // If Player found
        if (!is_null($player)) {
            $voucher = Voucher::where('code', $voucher_code)->first();

            // If Voucher not found
            if (is_null($voucher)) {
                $dump = true;
                $json["result"] = "Fail";
                $json["reason"] = "Error: Voucher data not found. Saved into Dump table.";
            }
        }
        // If Player not found
        else {
            $dump = true;
            $json["result"] = "Fail";
            $json["reason"] = "Error: Player data not found. Saved into Dump table.";
        }


        if ($dump) {
            // Save into dump table
            DB::table('voucher_distributions_data_dump')->insert(
                [
                    'voucher_code' => $voucher_code,
                    'email' => $email,
                    'redeem_code' => $redeem_code,
                    'created_at' => \Carbon\Carbon::now()
                ]
            );

        } else {
            // Create new voucehr distribution object
            $voucher_distribution = new VoucherDistribution();
            $voucher_distribution->voucher_id = $voucher->id;
            $voucher_distribution->player_id = $player->id;
            $voucher_distribution->redeem_code = $redeem_code;
            $voucher_distribution->created_at = $created_at;

            if ($voucher_distribution->save()) {
                $json["result"] = "Success";
            }
        }

        return response(json_encode($json), Response::HTTP_OK);
    }
}
