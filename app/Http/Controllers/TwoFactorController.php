<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class TwoFactorController extends Controller
{
    public function verifyForm()
    {
        return view('auth.2FA_Form');
    }

    public function verify(Request $request)
    {
        $request->validate([
            '2fa' => 'required',
        ]);

        if ($request->input('2fa') == Auth::user()->token_2fa) {
            $user = Auth::user();
            $user->token_2fa_expiry = \Carbon\Carbon::now()->addMinutes(config('session.lifetime'));
            $user->save();
            return redirect('dashboard');
        } else {
            return redirect('2FAForm')->with('message', 'Wrong authentication code. Please try again.');
        }
    }
}
