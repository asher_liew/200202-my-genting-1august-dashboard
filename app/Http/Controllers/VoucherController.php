<?php

namespace App\Http\Controllers;

use App\RedeemLocation;
use Auth;
use App\Voucher;
use App\VoucherDistribution;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class VoucherController extends Controller
{
    /**
     * Get the saved event ID from the cookie
     */
    public function getEventId()
    {
        $eventId = 1;
        if (isset($_COOKIE["eventID"])) {
            if ($_COOKIE["eventID"] != "") {
                $eventId = $_COOKIE["eventID"];
            }
        }

        return $eventId;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventId = self::getEventId();

        $vouchers = Voucher::where('event_id', $eventId)->get();
        $events = Event::all();

        return view('dashboard.voucher.manageVouchers', compact('vouchers'), compact('events'))
            ->with('i', 0)
            ->with('selectedEventId', $eventId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'description' => 'required',
        ]);

        $fileName = null;
        $extension = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = $image->getClientOriginalName();
            $extension = strtolower($request->image->extension());

            // If the file extension is not jpeg, jpg or png
            if ($extension != "jpeg" && $extension != "jpg" && $extension != "png") {
                return redirect()->route('manageVouchers.index')
                    ->withErrors(array('image' => 'Please upload valid image file'));
            }

            // Store the image file in
            $request->image->storeAs('images/voucher_images', $fileName, ['disk' => 'public']);
        }

        $voucher = array(
            'user_id' => Auth::user()->id,
            'event_id' => $request->event_id,
            'code' => $request->code,
            'description' => $request->description,
            'image' => $fileName,
        );

        Voucher::create($voucher);

        return redirect()->route('manageVouchers.index');
    }

    /**
     * Redeem the voucher.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function redemption(Request $request)
    {
        $outlet = $request->outlet;

        $check_outlet = RedeemLocation::where('outlet_code', $outlet)->first();

        if (!is_null($check_outlet)) {
            $request->session()->put('redeem_outlet', $check_outlet->outlet_code);
            return view('dashboard.voucher.redeemVouchers')->with('outlet', $outlet);
        } else {
            # Redeem outlet not found
            return redirect()->route('dashboard');
        }
    }

    /**
     * Show the details of the voucher based on the entered voucher code.
     *
     * @return \Illuminate\Http\Response
     */
    public function voucherDetails(Request $request)
    {
        $outlet_code = $request->redeem_location;
        $voucher_code = $request->voucher_code;

        $voucher_distribution = VoucherDistribution::where('voucher_code', $voucher_code)->first();
        $redeem_location = RedeemLocation::where('outlet_code', $outlet_code)->first();

        $voucher = null;

        # Check if this voucher code exists
        if ($voucher_distribution != null) {
            # Check if this voucher has been redeemed
            if ($voucher_distribution->redeem_location == null && $voucher_distribution->redeem_time == null) {
                $voucher = Voucher::where('id', $voucher_distribution->voucher_id)->first();

                if (!$redeem_location == null) {
                    $voucher_distribution->redeem_location_id = $redeem_location->id;
                    $voucher_distribution->redeem_time = \Carbon\Carbon::now();
                    $voucher_distribution->save();
                }

                return view('dashboard.voucher.voucherDetails')
                    ->with('redeem_location', $redeem_location)
                    ->with('voucher_code', $voucher_code)
                    ->with('voucher', $voucher);
            } else {
                return view('dashboard.voucher.duplicateRedemption')
                ->with('redeem_time', $voucher_distribution->redeem_time)
                ->with('outlet_code', $outlet_code);
            }
        } else {
            return view('dashboard.voucher.invalidVouchers')
            ->with('outlet_code', $outlet_code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function redeemedVoucherCode()
    {
        return view('dashboard.voucher.redeemedVoucherCode');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $voucher = Voucher::find($id);

        return view('dashboard.voucher.editVoucher', compact('voucher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code' => 'required',
            'description' => 'required',
        ]);

        $fileName = null;
        $extension = null;

        // If there is file sent
        // else check if is there a file previously
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = $image->getClientOriginalName();
            $extension = strtolower($request->image->extension());

            // If the file extension is not jpeg, jpg or png
            if ($extension != "jpeg" && $extension != "jpg" && $extension != "png") {
                return redirect()->route('manageVouchers.edit')
                    ->withErrors(array('image' => 'Please upload valid image file'));
            }

            // Store the image file in
            $request->image->storeAs('images/voucher_images', $fileName, ['disk' => 'public']);
        } else {
            if ($request->fileName != "") {
                $fileName = $request->fileName;
            }
        }

        $updatedVoucher = array(
            'user_id' => Auth::user()->id,
            'code' => $request->code,
            'description' => $request->description,
            'image' => $fileName,
        );

        $voucher = Voucher::find($id);
        $voucher->update($updatedVoucher);

        return redirect()->route('manageVouchers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voucher = Voucher::find($id);

        echo $voucher;

        $voucher->delete();

        return redirect()->route('manageVouchers.index');
    }

    /**
     * Display a listing of the deleted vouchers.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDeletedVouchers()
    {
        $eventId = self::getEventId();

        $vouchers = Voucher::onlyTrashed()
                    ->where('event_id', $eventId)
                    ->get();

        return view('dashboard.voucher.deletedVouchers')
            ->with('vouchers', $vouchers)
            ->with('i', 0);
    }

    /**
     * Restore a deleted voucher.
     *
     * @return \Illuminate\Http\Response
     */
    public function restoreVoucher(Request $request)
    {
        $voucherId = $request->voucherId;

        Voucher::onlyTrashed()
            ->where('id', $voucherId)
            ->restore();

        return redirect()->route('manageVouchers.index');
    }

    public function invalidVoucherCode(Request $request)
    {
        $voucher_code = $request->voucher_code;
        $invalid_voucher = VoucherDistribution::where('voucher_code', $voucher_code)->first();
    }
}
