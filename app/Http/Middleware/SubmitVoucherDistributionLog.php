<?php

namespace App\Http\Middleware;
use \Spatie\HttpLogger\DefaultLogWriter;

use Closure;

class SubmitVoucherDistributionLog implements \Spatie\HttpLogger\DefaultLogWriter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
