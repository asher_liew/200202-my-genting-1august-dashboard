<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TwoFactorVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::user();

            if ($user->token_2fa_expiry > \Carbon\Carbon::now()) {
                return $next($request);
            }

            $user->token_2fa = mt_rand(10000, 99999);
            $user->save();

            // Mail the user 2 FA token
            // Mail details
            $details = [
                'title' => 'Dashboard 2 Factor Authentication',
                'name' => $user->name,
                'code' => $user->token_2fa
            ];
            // Send mail
            \Mail::to($user->email)->send(new \App\Mail\Send2FAEmail($details));

            return redirect()->route('2FAForm');
        } else {
            // Redirect user to login page
            return redirect()->route('login');
        }
    }
}
