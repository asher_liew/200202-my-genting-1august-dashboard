<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class LicenseKey extends Model
{
    protected $table = 'license_keys';
    protected $fillable = ['license_key', 'machine_identity_code', 'license_key_expiry_date', 'isActivated'];
    // use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}