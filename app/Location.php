<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;
    
    protected $table = 'locations';

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function source_identifiers()
    {
        return $this->hasMany('App\SourceIdentifier');
    }
}
