<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';

    public function scores()
    {
        return $this->hasMany('App\Score');
    }

    public function voucher_distributions()
    {
        return $this->hasMany('App\VoucherDistribution');
    }

    public function references()
    {
        return $this->hasMany('App\Reference');
    }

    protected $fillable = [
        'name',
        'email',
        'contact',
        'age',
        'dob',
        'gender',
        'created_at'
    ];
}
