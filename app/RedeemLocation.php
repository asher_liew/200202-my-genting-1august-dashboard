<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedeemLocation extends Model
{
    protected $table = 'redeem_locations';

    protected $fillable = [
        'outlet_name',
        'outlet_code',
        'location',
        'description'
    ];
}
