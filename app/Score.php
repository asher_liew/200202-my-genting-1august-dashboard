<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $table = 'scores';

    public function player()
    {
        return $this->belongsTo('App\Player');
    }

    public function share()
    {
        return $this->hasMany('App\Share');
    }
    
    protected $fillable = [
        'player_id',
        'score',
        'result'
    ];
}
