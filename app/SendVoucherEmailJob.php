<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendVoucherEmailJob extends Model
{
    protected $table = 'send_voucher_email_jobs';

    public function player()
    {
        return $this->belongsTo('App\Player');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Voucher');
    }
}
