<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $table = 'shares';

    public function score(){
        return $this->belongsTo('App\Models\Score');
    }
    protected $fillable = [
        'score_id',
        'platform',
        'created_at'
    ];
}
