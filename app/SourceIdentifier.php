<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SourceIdentifier extends Model
{
    use SoftDeletes;
    
    protected $table = 'source_identifiers';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }
}
