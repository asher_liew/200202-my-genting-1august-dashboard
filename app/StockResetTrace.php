<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockResetTrace extends Model
{
    protected $table = 'stock_reset_traces';
}
