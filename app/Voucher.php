<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use SoftDeletes;

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function voucher_distributions()
    {
        return $this->hasMany('App\VoucherDistribution');
    }

    public function voucher_stocks()
    {
        return $this->hasOne('App\VoucherStock');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'event_id', 'code', 'description', 'image',
    ];
}
