<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherDistribution extends Model
{
    protected $table = 'voucher_distributions';

    public function player()
    {
        return $this->belongsTo('App\Player');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Voucher');
    }

    protected $fillable = [
        'voucher_id',
        'player_id',
        'redeem_code',
        'redeem_location_id',
        'redeem_time',
        'created_at',
        'redeemed_at'
    ];
}
