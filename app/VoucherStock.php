<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoucherStock extends Model
{
    protected $table = 'voucher_stocks';

    public function voucher()
    {
        return $this->belongsTo('App\Voucher');
    }

    protected $fillable = [
        'voucher_id',
        'amount'
    ];
}
