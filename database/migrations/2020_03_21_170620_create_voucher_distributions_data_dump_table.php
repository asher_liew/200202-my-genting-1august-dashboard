<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherDistributionsDataDumpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_distributions_data_dump', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('voucher_code')->nullable();
            $table->string('email')->nullable();
            $table->string('redeem_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_distributions_data_dump');
    }
}
