$(document).ready(function() {
    const today1 = moment().format('DD-MM-YYYY');
    const today2 = moment().format('DD MMMM YYYY');

    $('#datepicker').val(today1);
    $('#date-text').html(today2);

    const today3 = moment().format('YYYY-MM-DD');
    getRegistrationHourCount(today3);
})

// Actions after the user choose a date using the datepicker
// (to view the number of hourly registrations)
const picker = datepicker('#datepicker', {
    formatter: (input, date, instance) => {
        const value = moment(date).format('DD-MM-YYYY');

        input.value = value;
    },
    onSelect: (instance, date) => {
        // Do stuff when a date is selected (or unselected) on the calendar.
        // You have access to the datepicker instance for convenience.
        const value = moment(date).format('DD MMMM YYYY');
        const value2 = moment(date).format('YYYY-MM-DD');
        $('#date-text').html(value);

        getRegistrationHourCount(value2);
    }
})

// Send request to "getRegistrationHourCount" API to get the hourly registration
// count of the chosen date
function getRegistrationHourCount(date) {
    $.ajax({
        type: 'GET',
        url: '/getRegistrationHourCount',
        data: { date: date },
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
            response = JSON.parse(response)

            createRegistrationCountChart(response);
        }
    });
}

// Use the response from "getRegistrationHourCount" API to show the chart
function createRegistrationCountChart(data) {
    $('#hourly-registration-chart').remove();
    $('#chart-container').append('<canvas class="chart" id="hourly-registration-chart" height="300"></canvas>')

    var playTimeChartCtx = document.getElementById("hourly-registration-chart").getContext("2d");

    new Chart(playTimeChartCtx, {
        type: 'line',
        data: {
            labels: getTimes(),
            datasets: [{
                label: null,
                lineTension: 0,
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgb(255, 233, 244, .5)',
                data: data
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        precision: 0
                    }
                }]
            }
        }
    });
}

// To get the list of time from 12am to 11pm
function getTimes() {
    var timeList = ["12am"];

    for (var i = 1; i < 12; i++) {
        timeList.push(i + "am");
    }

    timeList.push("12pm");

    for (var i = 1; i < 12; i++) {
        timeList.push(i + "pm");
    }

    return timeList;
}
