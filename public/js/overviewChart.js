(function () {
    // getDailyOverviewList(tableName)
    // getWeeklyOverviewList(tableName)
    getBranchDailyOverviewList()
})();

function getDailyOverviewList(tableName) {
    $.ajax({
        type: 'GET',
        url: '/getDailyOverview',
        data: { tableName: tableName },
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            var result = JSON.parse(response)
            var dailyOverviewDates = []
            var dailyOverviewDateCounts = []

            result.forEach(element => {
                dailyOverviewDates.push(element.date)
                dailyOverviewDateCounts.push(element.count)
            });

            createDailyOverviewChart(dailyOverviewDates, dailyOverviewDateCounts)
        }
    })
}

function getWeeklyOverviewList() {
    $.ajax({
        type: 'GET',
        url: '/getWeeklyOverview',
        data: { tableName: tableName },
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            var result = JSON.parse(response)
            var weeklyOverviewWeeks = []
            var weeklyOverviewDateCounts = []

            result.forEach(element => {
                weeklyOverviewWeeks.push(element.week)
                weeklyOverviewDateCounts.push(element.count)
            });

            var firstWeek = 1
            for (var i = 0; i < weeklyOverviewWeeks.length; i++) {
                if (i == 0) {
                    firstWeek = weeklyOverviewWeeks[i] - 1
                }

                weeklyOverviewWeeks[i] = weeklyOverviewWeeks[i] - firstWeek
            }

            createWeeklyOverviewChart(weeklyOverviewWeeks, weeklyOverviewDateCounts)
        }
    })
}

function getBranchDailyOverviewList() {
    $.ajax({
        type: 'GET',
        url: '/getBranchDailyOverview',
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            var result = JSON.parse(response)

            var dateList = []

            var branchList = []
            result.branchList.forEach(element => {
                branchList.push(element.location)
            });

            result.branchDailyOverviewList.forEach(element => {
                // console.log(element)
                if (!dateList.includes(element.date)) {
                    dateList.push(element.date)
                }
            });

            dateList.sort()

            var branchOverviewMatrix = []

            // Create the second dimention of the branchOverviewMatrix based on the length of dateList
            for (var i = 0; i < branchList.length; i++) {
                branchOverviewMatrix[i] = []
            }

            // Initialize the branchOverviewMatrix
            for (var i = 0; i < branchList.length; i++) {
                for (var j = 0; j < dateList.length; j++) {
                    branchOverviewMatrix[i][j] = 0
                }
            }

            for (var i = 0; i < branchList.length; i++) {
                for (var j = 0; j < dateList.length; j++) {
                    result.branchDailyOverviewList.forEach(element => {
                        if (element.location === branchList[i] && element.date === dateList[j]) {
                            branchOverviewMatrix[i][j] = element.count
                        }
                    });
                }
            }

            var datasets = []
            var chartDataset

            for (var i = 0; i < branchList.length; i++) {
                chartDataset = new BarChartDataset(branchList[i], generateRandomColor(), branchOverviewMatrix[i])
                datasets.push(chartDataset)
            }

            createBranchDailyOverviewChart(dateList, datasets)
        }
    })
}

function createDailyOverviewChart(labels, data) {
    var dailyOverviewChartCtx = document.getElementById("daily-overview-chart").getContext("2d");

    new Chart(dailyOverviewChartCtx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: null,
                lineTension: 0,
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgb(255, 233, 244, .5)',
                data: data
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        precision: 0
                    }
                }]
            }
        }
    });
}

function createWeeklyOverviewChart(labels, data) {
    var dailyOverviewChartCtx = document.getElementById("weekly-overview-chart").getContext("2d");

    new Chart(dailyOverviewChartCtx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: null,
                backgroundColor: 'rgb(255, 99, 132)',
                data: data
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        precision: 0
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Week'
                    }
                }]
            }
        }
    });
}

function createBranchDailyOverviewChart(labels, datasets) {
    var branchDailyOverviewChartCtx = document.getElementById("branch-daily-overview-chart").getContext("2d");

    new Chart(branchDailyOverviewChartCtx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    })
}

function shuffle(array) {
    var currentIndex = array.length;
    var temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function generateRandomColor() {
    // The available hex options
    var hex = ['a', 'b', 'c', 'd', 'e', 'f', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var color = '#';

    // Create a six-digit hex color
    for (var i = 0; i < 6; i++) {

        // Shuffle the hex values
        shuffle(hex);

        // Append first hex value to the string
        color += hex[0];

    }

    // Return the color string
    return color;
}

class BarChartDataset {
    constructor(label, fillColor, data) {
        this.label = label
        this.backgroundColor = fillColor
        this.borderColor = fillColor
        this.data = data
    }
}
