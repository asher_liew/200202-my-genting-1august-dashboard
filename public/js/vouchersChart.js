(function () {
    getDailyOverviewList()
})();

function getDailyOverviewList() {
    $.ajax({
        type: 'GET',
        url: '/getVoucherDailyOverview',
        headers:
        {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            buildCharts(JSON.parse(response))
        }
    })
}

function buildCharts(data) {
    var totalLabelListTemp = []
    var redeemedLabelList = []
    var unredeemedLabelList = []

    var totalCountList = []
    var redeemedCountList = []
    var unredeemedCountList = []

    data.forEach(element => {
        totalLabelListTemp.push(element.date)

        if (element.location !== null && element.time !== null) {
            redeemedLabelList.push(element.date)
        } else {
            unredeemedLabelList.push(element.date)
        }
    });

    // Remove duplicate values in totalLabelListTemp to get the day list
    var totalLabelList = []

    totalLabelListTemp.forEach(element => {
        if (!totalLabelList.includes(element)) {
            totalLabelList.push(element)
        }
    });

    // Initialize the count lists
    for (var day = 0; day < totalLabelList.length; day++) {
        totalCountList[day] = 0
        redeemedCountList[day] = 0
        unredeemedCountList[day] = 0
    }

    // Calculate the count of total, redeemed and unredeemed of each day
    for (var day = 0; day < totalLabelList.length; day++) {
        totalLabelListTemp.forEach(element => {
            if (element === totalLabelList[day]) {
                totalCountList[day]++
            }
        });

        redeemedLabelList.forEach(element => {
            if (element === totalLabelList[day]) {
                redeemedCountList[day]++
            }
        });

        unredeemedLabelList.forEach(element => {
            if (element === totalLabelList[day]) {
                unredeemedCountList[day]++
            }
        });
    }

    // Get the total number of redeemed and unredeemed for the pie chart
    var redeemedCount = 0
    var unredeemedCount = 0

    redeemedCountList.forEach(element => {
        redeemedCount += element
    });

    unredeemedCountList.forEach(element => {
        unredeemedCount += element
    });

    createDailyOverviewChart(totalLabelList, totalCountList, redeemedCountList, unredeemedCountList)
    createPieChart(redeemedCount, unredeemedCount)
}

function createDailyOverviewChart(totalLabelList, totalCountList, redeemedCountList, unredeemedCountList) {
    var dailyOverviewChartCtx = document.getElementById("daily-overview-chart").getContext("2d");

    new Chart(dailyOverviewChartCtx, {
        type: 'line',
        data: {
            labels: totalLabelList,
            datasets: [{
                label: 'Total Distributed',
                lineTension: 0,
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgb(255, 99, 132)',
                fill: false,
                data: totalCountList
            },
            {
                label: 'Redeemed',
                lineTension: 0,
                borderColor: 'rgb(65, 105, 225)',
                backgroundColor: 'rgb(65, 105, 225)',
                fill: false,
                data: redeemedCountList
            },
            {
                label: 'Unredeemed',
                lineTension: 0,
                borderColor: 'rgb(60,179,113)',
                backgroundColor: 'rgb(60,179,113)',
                fill: false,
                data: unredeemedCountList
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        precision: 0
                    }
                }]
            }
        }
    });
}

function createPieChart(redeemedCount, unredeemedCount) {
    var pieChartCtx = document.getElementById("pie-chart").getContext("2d");

    new Chart(pieChartCtx, {
        type: 'pie',
        data: {
            labels: ['Redeemed', 'Unredeemed'],
            datasets: [{
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [redeemedCount, unredeemedCount]
            }]
        }
    })
}
