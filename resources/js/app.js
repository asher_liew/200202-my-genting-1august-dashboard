global.$ = global.jQuery = require('jquery');
require('popper.js');
require('bootstrap');
require('pace');
require('perfect-scrollbar');
require('@coreui/coreui');
require('chart.js');
global.datepicker = require('js-datepicker');
global.moment = require('moment');
global.Vue = require('vue');
global.axios = require('axios');

import EventDropdown from './components/EventDropdown.vue'
import SourceIdentifierDropdown from './components/SourceIdentifierDropdown.vue'

new Vue({
    el: '#eventDropdown',
    components: {
        EventDropdown
    },
    render: h => h(EventDropdown)
})

new Vue({
    el: '#sourceIdentifierDropdown',
    components: {
        SourceIdentifierDropdown
    },
    render: h => h(SourceIdentifierDropdown)
})
