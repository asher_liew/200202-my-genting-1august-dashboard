@extends('layouts.app')

@section('content')
<div class="container-fluid mt-5 pt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session('message'))
                <div class="alert alert-danger">
                    {{ session('message') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('2 Factor Authenticator') }}</div>

                <div class="card-body">
                    <form method="POST" action="/2FAVerify">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Authentication code') }}</label>

                            <div class="col-md-6">

                                <input id="2fa" type="text" class="form-control {{ $errors->has('token') ? ' is-invalid' : '' }}" name="2fa" placeholder="Enter the code you received here." required autofocus>

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection