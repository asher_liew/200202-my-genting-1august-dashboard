<div class="row mb-5"></div>
<footer class="app-footer navbar fixed-bottom">
    <div class="mx-auto">
        <a href="https://uid.asia" target="_blank">
            Unicom Interactive Digital
        </a>
        <span>
            © 2019 Unicom Interactive Digital (UID).
        </span>
    </div>
</footer>
