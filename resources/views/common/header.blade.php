<header class="app-header navbar navbar-expand-lg">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" data-toggle="sidebar-show" type="button">
        <span class="navbar-toggler-icon">
        </span>
    </button>
    <a class="navbar-brand" href="/dashboard">
        <img alt="CoreUI Logo" class="navbar-brand-full ml-3" src="{{ asset('img/brand/logo.png') }}" width="100%" />
        <img alt="CoreUI Logo" class="navbar-brand-minimized ml-3" src="{{ asset('img/brand/logo-small.png') }}" width="100%" />
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" data-toggle="sidebar-lg-show" type="button">
        <span class="navbar-toggler-icon">
        </span>
    </button>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">
        </span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @if (Auth::check())
        <ul class="navbar-nav mr-auto ml-5">
            <li class="nav-item">
                <a class="nav-link disabled label-event" href="#">Event :</a>
            </li>
            <li class="nav-item dropdown">
                <div id="eventDropdown"></div>
            </li>
            <li class="nav-item dropdown ml-3">
                <div id="sourceIdentifierDropdown"></div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto ul-user">
            <div class="nav-item dropdown">
                <a id="dropdownMenuLink" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }}
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item d-none" href="#">
                        <i class="icon-wrench text-dark">
                        </i>
                        Settings
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="icon-power text-dark">
                        </i>
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </ul>
        @endif
    </div>

</header>
