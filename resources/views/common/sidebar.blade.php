<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/dashboard">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
            <li class="nav-title">DATA</li>
            <li class="nav-item nav-dropdown d-none">
                <a class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-user"></i> Players
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/players">
                            <i class="nav-icon icon-people"></i> Database
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/scores">
                            <i class="nav-icon icon-game-controller"></i> Scores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/shares">
                            <i class="nav-icon icon-share"></i> Shares
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle">
                    <i class="nav-icon icon-present"></i> Vouchers
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/vouchers">
                            <i class="nav-icon icon-layers"></i> Database
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/voucherOverview">
                            <i class="nav-icon icon-chart"></i> Overview
                        </a>
                    </li>
                    <li class="nav-item d-none">
                        <a class="nav-link" href="{{ route('manageVouchers.index') }}">
                            <i class="nav-icon icon-wrench"></i> Manage
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
