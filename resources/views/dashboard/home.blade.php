@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn">
                <input id="total-visit-count" type="hidden">
                <input id="total-unique-visit-count" type="hidden">
                <div class="row">
                    <div class="col-sm-6 col-lg-3 d-none">
                        <div class="card text-white bg-primary">
                            <div class="card-body">
                                <i class="icon-user-follow p-0 float-right"></i>
                                <div id="player-count" class="text-value">{{ $playerCount }}</div>
                                <div>Total Players</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="card text-white bg-pink">
                            <div class="card-body">
                                <div class="btn-group float-right">
                                    <i class="icon-present"></i>
                                </div>
                                <div id="distributed-voucher-count" class="text-value">{{ $totalDistVoucher }}</div>
                                <div>Vouchers Distributed</div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-->
                    <div class="col-sm-6 col-lg-3 d-none">
                        <div class="card text-white bg-orange">
                            <div class="card-body">
                                <div class="btn-group float-right">
                                    <i class="icon-envelope-letter"></i>
                                </div>
                                <div class="text-value">{{ $totalClaimedVoucher }}</div>
                                <div>Example 2</div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4>Voucher Distribution Overview</h4>
                        <div class="row mt-4">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <h5 class="card-title mb-0">Number of Voucher Distributions (Hourly)</h5>
                                                <div id="date-text" class="small text-muted">November 2017</div>
                                            </div>
                                            <!-- /.col-->
                                            <div class="col-sm-3 d-md-block">
                                                <div class="btn-group btn-group-toggle float-right mr-2"
                                                    data-toggle="buttons">
                                                    <input id="datepicker"
                                                        class="text-center form-control cursor-pointer" type="text" />
                                                </div>
                                            </div>
                                            <!-- /.col-->
                                        </div>
                                        <!-- /.row-->
                                    </div>
                                    <div class="card-body">
                                        <div id="chart-container" class="chart-wrapper" style="height:350px; margin-top:40px;">
                                            <canvas class="chart" id="hourly-registration-chart" height="300"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-->
                            </div>
                        </div>
                        <div class="row d-none">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <h5 class="card-title mb-0">Weekly Overview</h5>
                                            </div>
                                            <!-- /.col-->
                                            <div class="col-sm-3 d-md-block">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle float-right mr-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      Week
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                        @for ($i = 0; $i < sizeof($playersYearAndWeek); $i++)
                                                        <a class="dropdown-item" onclick="selectWeek({{ $playersYearAndWeek[$i]->year }}, {{ $playersYearAndWeek[$i]->week }})">{{ ($i + 1) }}</a>
                                                        @endfor
                                                    </div>
                                                  </div>
                                            </div>
                                            <!-- /.col-->
                                        </div>
                                        <!-- /.row-->
                                        <p>{{ $playersYearAndWeek[0]->week }}</p>
                                    </div>
                                    <div class="card-body">
                                        <canvas class="chart" id="weekly-overview-chart" height="100"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-->
            <div class="card d-none">
                <div class="card-body">
                    <h4>Registration Overview per Branch</h4>
                    <div class="row mt-4 justify-content-center">
                        <div class="col-12">
                            <canvas class="chart" id="branch-daily-overview-chart" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card d-none">
                <div class="card-body">
                    <h4>Social Sharing Overview</h4>
                    <div class="row mt-4">
                        <div class="col-sm-6 col-lg-3">
                            <div class="brand-card">
                                <div class="brand-card-header bg-flickr">
                                    <i class="fa fa-share"></i>
                                </div>
                                <div class="brand-card-body">
                                    <div>
                                        <div class="text-value">{{ $shareInfo["totalShare"] }}</div>
                                        <div class="text-uppercase text-muted small">Total Shares</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-3">
                            <div class="brand-card">
                                <div class="brand-card-header bg-facebook">
                                    <i class="fab fa-facebook-f"></i>
                                </div>
                                <div class="brand-card-body">
                                    <div>
                                        <div class="text-value">{{ $shareInfo["totalShareFB"] }}</div>
                                        <div class="text-uppercase text-muted small">shares</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-3">
                            <div class="brand-card">
                                <div class="brand-card-header bg-twitter">
                                    <i class="fab fa-twitter"></i>
                                </div>
                                <div class="brand-card-body">
                                    <div>
                                        <div class="text-value">{{ $shareInfo["totalShareTW"] }}</div>
                                        <div class="text-uppercase text-muted small">shares</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                        <div class="col-sm-6 col-lg-3">
                            <div class="brand-card">
                                <div class="brand-card-header bg-whatsapp">
                                    <i class="fab fa-whatsapp"></i>
                                </div>
                                <div class="brand-card-body">
                                    <div>
                                        <div class="text-value">{{ $shareInfo["totalShareWA"] }}</div>
                                        <div class="text-uppercase text-muted small">shares</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-->
                    </div>
                </div>
            </div>
        </div>
</div>
</main>
</aside>
</div>

@section('script')
<script src="js/datePicker.js"></script>
<script src="js/weekPicker.js"></script>

@endsection

<script type="text/javascript">
    var tableName = "players";
</script>

@endsection
