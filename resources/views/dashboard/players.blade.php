@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="float-right">
                            <a href="/downloadPlayersCSV" class="btn btn-primary">Download CSV file</a>
                        </div>
                        <div class="table-responsive mt-5">
                            <table id="data-table" class="table table-striped table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Age</th>
                                        <th>D.O.B</th>
                                        <th>Gender</th>
                                        <th>Event</th>
                                        <th>Location</th>
                                        <th>Registered At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($players as $player)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $player->name }}</td>
                                        <td>{{ $player->contact }}</td>
                                        <td>{{ $player->email }}</td>
                                        <td>{{ $player->age }}</td>
                                        <td>{{ $player->dob }}</td>
                                        <td>{{ $player->gender }}</td>

                                        @if ($player->event_code != "")
                                        <td>{{ $player->event_code }}</td>
                                        @else
                                        <td>-</td>
                                        @endif

                                        @if ($player->description != "")
                                        <td>{{ $player->description }}</td>
                                        @else
                                        <td>-</td>
                                        @endif

                                        <td>{{ $player->created_at }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@endsection
