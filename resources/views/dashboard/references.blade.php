@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <div class="card">
                            <div class="card-header">
                                Daily Overview
                            </div>
                            <div class="card-body">
                                <canvas class="chart" id="daily-overview-chart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <div class="card">
                            <div class="card-header">
                                Weekly Overview
                            </div>
                            <div class="card-body">
                                <canvas class="chart" id="weekly-overview-chart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="float-right">
                            <a href="/downloadReferencesCSV" class="btn btn-primary">Download CSV file</a>
                        </div>
                        <div class="table-responsive mt-5">
                            <table id="references-table" class="table table-striped table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Player Name</th>
                                        <th>Referral Name</th>
                                        <th>Referral Contact</th>
                                        <th>Referral Email</th>
                                        <th>Reference Code</th>
                                        <th>Referred At</th>
                                        <th>Email Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($references as $reference)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $reference->name }}</td>
                                        <td>{{ $reference->referral_name }}</td>
                                        <td>{{ $reference->referral_contact }}</td>
                                        <td>{{ $reference->referral_email }}</td>
                                        <td>{{ $reference->reference_code }}</td>
                                        <td>{{ $reference->datetime }}</td>

                                        @if ($reference->isEmailSent == 1)
                                            <td>Sent</td>
                                        @else
                                            <td>Pending</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@section('referencesScript')
    <script type="text/javascript" src="js/datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#references-table').DataTable({
                "displayLength": 5
            });
        });
    </script>
@endsection

<script type="text/javascript">
    var dailyOverviewDates = <?php echo json_encode($dailyOverviewDates); ?>;
    var dailyOverviewDateCounts = <?php echo json_encode($dailyOverviewDateCounts); ?>;
    var weeklyOverviewWeeks = <?php echo json_encode($weeklyOverviewWeeks); ?>;
    var weeklyOverviewWeekCounts = <?php echo json_encode($weeklyOverviewWeekCounts); ?>;
</script>

@endsection
