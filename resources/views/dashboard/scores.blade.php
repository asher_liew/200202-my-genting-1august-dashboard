@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn">
                <div class="card">
                    <div class="card-body">
                        <div class="float-right">
                            <a href="/downloadScoresCSV" class="btn btn-primary">Download CSV file</a>
                        </div>
                        <div class="table-responsive mt-5">
                            <table id="data-table" class="table table-striped table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Player Name</th>
                                        <th>Score</th>
                                        <th>Result</th>
                                        <th>Played At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($scores as $score)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $score->name }}</td>
                                        <td>{{ $score->score }}</td>
                                        <td>{{ $score->result }}</td>
                                        <td>{{ $score->created_at }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@endsection
