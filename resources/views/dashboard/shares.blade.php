@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn">
                <div class="card">
                    <div class="card-body">
                        <div class="float-right">
                            <a href="/downloadSharesCSV" class="btn btn-primary">Download CSV file</a>
                        </div>
                        <div class="table-responsive mt-5">
                            <table id="data-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Player Name</th>
                                        <th>Platform</th>
                                        <th>Shared At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($shares as $share)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $share->name }}</td>
                                            <td>{{ $share->platform }}</td>
                                            <td>{{ $share->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@endsection
