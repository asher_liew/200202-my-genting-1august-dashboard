@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                List of Deleted Vouchers
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Image</th>
                                                <th>Restore</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($vouchers as $voucher)
                                            @php
                                            $img = asset("/images/voucher_images/" . $voucher->image);
                                            @endphp
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $voucher->code }}</td>
                                                <td>{{ $voucher->description }}</td>
                                                <td><img src="{{ $img }}" alt="Voucher Image" style="width: 50%"></td>
                                                <td>
                                                    <form id="restore-voucher-form" method="POST"
                                                        action="{{ action('VoucherController@restoreVoucher') }}"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <input type="hidden" name="voucherId" value="{{ $voucher->id }}">
                                                        <button type="submit" class="btn btn-success">
                                                            <i class="icon-action-undo"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-5">
                    <a href="/manageVouchers" class="btn btn-primary text-white m-auto">Back</a>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="deleteVoucherModal" tabindex="-1" role="dialog" aria-labelledby="deleteVoucherModal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Voucher</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete voucher with code "<b id="voucherCode"></b>"?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="deleteBtn" type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@endsection
