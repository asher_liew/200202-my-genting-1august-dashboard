@extends('layouts.voucherRedemption')
@section('content')

<main>
    <div class="container col-sm-6">
        <div class="animated fadeIn text-center">
            <div class="card mt-5">
                    <div class="card-body">
                    <h1 class="text-header" style="padding-bottom:5%;">Duplicate Redemption Vouchers!</h1>
                    <p class="alert alert-danger">Sorry, this voucher has been redeemed at<br>{{ $redeem_time }} </p>
                    <a class="btn btn-dark" href="/redeemVoucher/outlet/<?=$outlet_code; ?>">Go Back</a>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
