@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-lg-8">
                        <div class="card">
                            <div class="card-header">
                                Edit Voucher
                            </div>
                            <div class="card-body">
                                <form action="{{ route('manageVouchers.update', $voucher->id) }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <label for="code-input">Code</label>
                                        <input type="text" class="form-control text-center" id="code-input" name="code"
                                            value="{{ $voucher->code }}">
                                        @error('code')
                                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="desc-input">Description</label>
                                        <textarea class="form-control text-center" id="desc-input" rows="2"
                                            name="description" value="fsa">{{ $voucher->description }}</textarea>
                                        @error('description')
                                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <p>Image</p>
                                        <img src="{{ asset('/images/voucher_images/' . $voucher->image) }}"
                                            alt="Voucher Image" style="width: 80%">
                                    </div>
                                    <div class="form-group">
                                        <label><small>Update image:</small></label>
                                        <div class="custom-file">
                                            <input type="hidden" name="fileName" value="{{ $voucher->image }}">
                                            <input type="file" class="custom-file-input" id="image-input" name="image">
                                            <label class="custom-file-label" for="image-input">Choose file</label>
                                            @error('image')
                                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="text-center mt-4">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@section('fileInputScript')
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection

@endsection
