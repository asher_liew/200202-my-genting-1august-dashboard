@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-lg-6">
                        <div class="card">
                            <div id="accordion" class="card-header" data-toggle="collapse"
                                data-target="#create-voucher-div" aria-expanded="true"
                                aria-controls="create-voucher-div">
                                Add Voucher
                                <span><i class="icon-arrow-down drop-down-arrow"></i></span>
                            </div>
                            <div id="create-voucher-div" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    <form method="POST" action="{{ route('manageVouchers.store') }}"
                                        enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label for="event-select">Event</label>
                                            <select class="form-control" id="event-select" name="event_id">
                                                @foreach ($events as $event)
                                                    @if ($event->id == $selectedEventId)
                                                    <option selected value="{{ $event->id }}">{{ $event->event_code }}</option>
                                                    @else
                                                    <option value="{{ $event->id }}">{{ $event->event_code }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="code-input">Code</label>
                                            <input type="text" class="form-control text-center" id="code-input"
                                                name="code">
                                            @error('code')
                                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="desc-input">Description</label>
                                            <textarea class="form-control text-center" id="desc-input" rows="2"
                                                name="description"></textarea>
                                            @error('description')
                                            <div class="alert alert-danger mt-1">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Image</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="image-input"
                                                    name="image">
                                                <label class="custom-file-label" for="image-input">Choose file</label>
                                                @error('image')
                                                <div class="alert alert-danger mt-1">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                List of Vouchers
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Code</th>
                                                <th>Description</th>
                                                <th>Image</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($vouchers as $voucher)
                                            @php
                                            $img = asset("/images/voucher_images/" . $voucher->image);
                                            @endphp
                                            <tr>
                                                <td>{{ ++$i }}</td>
                                                <td>{{ $voucher->code }}</td>
                                                <td>{{ $voucher->description }}</td>
                                                <td><img src="{{ $img }}" alt="Voucher Image" style="width: 50%"></td>
                                                <td>
                                                    <button type="button" class="btn btn-primary"
                                                        onclick="location.href='{{ route('manageVouchers.edit', $voucher->id) }}'">
                                                        <i class="icon-note"></i>
                                                    </button>
                                                </td>
                                                <td>
                                                    @php
                                                    $formId = "form" . $voucher->id;
                                                    @endphp
                                                    <form id="{{ $formId }}"
                                                        action="{{ route('manageVouchers.destroy', $voucher->id) }}"
                                                        method="POST" class='test'>
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="button" class="btn btn-danger"
                                                            data-voucher="{{ $voucher->code }}"
                                                            data-voucherId="{{ $voucher->id }}"
                                                            onclick="deleteVoucher(this)">
                                                            <i class="icon-trash"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-5">
                    <a href="/viewDeletedVouchers" class="btn btn-primary text-white m-auto">Restore Deleted
                        Vouchers</a>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="deleteVoucherModal" tabindex="-1" role="dialog" aria-labelledby="deleteVoucherModal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Voucher</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete voucher with code "<b id="voucherCode"></b>"?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button id="deleteBtn" type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@section('fileInputScript')
<script>
    function deleteVoucher(btn) {
        var voucherId = btn.getAttribute("data-voucherId");
        var voucherCode = btn.getAttribute("data-voucher");

        document.getElementById("voucherCode").innerHTML = voucherCode;
        document.getElementById("deleteBtn").setAttribute("data-voucherId", voucherId);

        $('#deleteVoucherModal').modal('show');
    }

    $('#deleteBtn').on('click', function() {
        var voucherId = $(this).attr('data-voucherId');
        var formId = "form" + voucherId;

        document.getElementById(formId).submit();
    });

    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('.collapse').on('show.bs.collapse', function() {
        $(this).siblings('.card-header').addClass('active');
    });

    $('.collapse').on('hide.bs.collapse', function() {
        $(this).siblings('.card-header').removeClass('active');
    });
</script>
@endsection

@endsection
