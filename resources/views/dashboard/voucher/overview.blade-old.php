@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <div class="card">
                            <div class="card-header">
                                Daily Overview
                            </div>
                            <div class="card-body">
                                <canvas class="chart" id="daily-overview-chart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 d-none">
                        <div class="card">
                            <div class="card-header">
                                Redemption Status
                            </div>
                            <div class="card-body">
                                <canvas class="chart" id="pie-chart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                Overall Overview by Voucher Code
                            </div>
                            <div class="card-body">
                                <div class="d-flex flex-wrap">
                                    @for ($i = 0; $i < sizeof($totalDistributedPerVoucherList); $i++) 
                                    <div class="col-sm-6 col-lg-3">
                                        <div class="brand-card text-white">
                                            <div class="brand-card-header bg-gray-dark">
                                                 <b>{{ $totalDistributedPerVoucherList[$i]->code }}</b>
                                            </div>
                                            <div class="brand-card-body bg-gray">
                                                <div>
                                                    <div class="text-value">{{ $totalDistributedPerVoucherList[$i]->count }}</div>
                                                    <div class="text-muted small">Total</div>
                                                </div>
                                                <div>
                                                    <div class="text-value">{{ $redeemedPerVoucherList[$i]->count }}</div>
                                                    <div class="text-muted small">Redeemed</div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Daily Overview by Voucher Code</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Date</th>

                                    @foreach ($dailyOverviewDateList as $item)
                                    <th>{{ $item }}</th>
                                    @endforeach
                                </tr>

                                <tr>
                                 <th>GPMA</th>
 
                                    @foreach ($dailyOverviewPerDay as $size) 
                                    <td>{{ $size->count }}</td>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
</main>
</div>

@endsection
