@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                        <div class="card">
                            <div class="card-header">
                                Daily Overview
                            </div>
                            <div class="card-body">
                                <canvas class="chart" id="daily-overview-chart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 d-none">
                        <div class="card">
                            <div class="card-header">
                                Redemption Status
                            </div>
                            <div class="card-body">
                                <canvas class="chart" id="pie-chart" height="100"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Daily Overview by Voucher Code</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Date</th>

                                    @foreach ($dailyOverviewDateList as $item)
                                    <th>{{ $item }}</th>
                                    @endforeach
                                </tr>

                                <tr>
                                 <th>GPMA</th>
 
                                    @foreach ($dailyOverviewPerDay as $size) 
                                    <td>{{ $size->count }}</td>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</div>
</main>
</div>

@endsection
