@extends('layouts.voucherRedemption')
@section('content')

<main>
    <div class="container-fluid">
        <div class="w-100 animated fadeIn text-center">
            <div class="row">
                <div class="col-sm-12 col-lg-8 m-auto">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="text-theme">Voucher Redemption</h4>
                            <p class="card-text mt-3 text-theme-secondary">Enter the voucher code to redeem.</p>
                            <form method="POST" action="{{ action('VoucherController@voucherDetails') }}" enctype="multipart/form-data" id="voucher-code-form" class="w-75 ml-auto mr-auto mt-5">
                                @csrf

                                <div class="form-group">
                                    <label class="text-theme" for="redeem_location-input">Outlet</label>
                                    <input type="text" class="form-control text-center input-field" name="redeem_location" id="redeem_location-input" value="{{ $outlet }}" readonly>
                                </div>
                                <div class="form-group mt-4">
                                    <label class="text-theme" for="voucher_code-input">Voucher Code</label>
                                    <input type="text" class="form-control text-center input-field" name="voucher_code" id="voucher_code-input">
                                </div>
                                <div class="mt-5">
                                    <button type="submit" class="btn btn-theme">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
