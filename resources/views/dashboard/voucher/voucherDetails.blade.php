@extends('layouts.voucherRedemption')
@section('content')

<main>
    <div class="container">
        <div class="animated fadeIn text-center">
            <div class="row">
                <div class="col-sm-12 col-lg-8 m-auto">
                    <div class="card mt-5">
                        <div class="card-body">
                            <h1 class="text-header" style="padding-bottom:5%;">Congratulations! </h1>
                            <h4 class="card-title">{{ $voucher->code }}</h4>
                            <p class="card-text mt-4 text-theme">{{ $voucher->description }}</p>
                            <p class="card-text mt-4"><small class="text-theme-secondary">Redeemed at
                                    {{ Carbon\Carbon::now() }}</small></p>
                            @php
                            $img = asset("/images/voucher_images/" . $voucher->image);
                            @endphp
                            <img id="voucherImg" src="{{ $img }}" alt="Voucher Image" class="card-img-bottom voucher-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    window.onload = function() {
        var img = document.getElementById("voucherImg");

        if (img.clientHeight > img.clientWidth) {
            img.classList.add("voucher-img-tall");
        } else {
            img.classList.add("voucher-img-wide");
        }
    }
</script>

@endsection