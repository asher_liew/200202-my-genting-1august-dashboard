@extends('layouts.app')
@section('content')

<div class="app-body">
    @include('common.sidebar')

    <main class="main">
        <div class="container-fluid mt-4">
            <div class="animated fadeIn text-center">
                <div class="card">
                    <div class="card-body">
                        <div class="float-right">
                            <a href="/downloadVouchersCSV" class="btn btn-primary">Download CSV file</a>
                        </div>
                        <div class="table-responsive mt-5">
                            <table id="data-table" class="table table-striped table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th></th>
                                        {{-- <th>Player Name</th>
                                        <th>Phone Number</th> --}}
                                        <th>Redemption Code</th>
                                        <!-- <th>Voucher Code</th> -->
                                        <th>Source</th>
                                        <th>Distributed At</th>
                                        {{-- <th>Redemption Status</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($voucherDistributions as $voucherDistribution)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            {{-- <td>{{ $voucherDistribution->name }}</td>
                                            <td>{{ $voucherDistribution->contact }}</td> --}}
                                            <td>{{ $voucherDistribution->redeem_code }}</td>
                                            <!-- <td>{{ $voucherDistribution->voucherCode }}</td> -->
                                            <td>{{ $voucherDistribution->source }}</td>
                                            <td>{{ $voucherDistribution->created_at }}</td>

                                            {{-- @if ($voucherDistribution->redeem_location_id != null && $voucherDistribution->redeem_time != null)
                                            <td>Redeemed</td>
                                            @else
                                            <td>Unredeemed</td>
                                            @endif --}}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

@endsection
