<!DOCTYPE html>
<html>
<head>
    <title>Game Dashboard 2 Factor Authentication</title>
</head>
<body>
    <h1>{{ $details['title'] }}</h1>
    <p>Greetings {{ $details['name'] }},</p>
    <p>Please use this code to continue with the login: {{ $details['code'] }}</p>
   
    <p>Thank you</p>
</body>
</html>