<!DOCTYPE html>
<html lang="en">

<head>
    <base href="./admin">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Laravel">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Game Dashboard</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

    @include('common.header')

    @yield('content')

    @include('common.footer')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>

    <!-- Include the script based on the current page -->
    @if (Request::path() !== 'dashboard')
    <script>
        $(document).ready( function () {
            $('#data-table').DataTable({
                "displayLength": 25
            });
        });
    </script>
    @endif

    <!-- Include the script only on player list page -->
    @if (Request::path() === 'dashboard')
    <script src="{{ asset('js/overviewChart.js') }}"></script>
    @endif

    <!-- Include the script only on voucher list pages -->
    @if (Request::path() === 'voucherOverview')
    <script src="{{ asset('js/vouchersChart.js') }}"></script>
    @endif

    <!-- Include the script only on manage vouchers page -->
    @if (Request::path() === 'manageVouchers' || Request::is('manageVouchers/*/edit'))
    @yield('fileInputScript');
    @endif

    @yield('script');

</body>

</html>
