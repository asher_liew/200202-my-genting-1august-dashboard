<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*****************************************************
 *****************  Internal APIs  *******************
 ****************************************************/

// [POST] API to get the list of events
// Local: http://127.0.0.1:10001/api/get-event-list
// Server: http://domain.com/public/api/get-event-list
Route::get('get-event-list', 'APIController@getEventList');

// [GET] API to get the list of source identifiers within the dashboard
// Local: http://127.0.0.1:10001/api/get-source-identifier-list-internal
// Server: http://domain.com/public/api/get-source-identifier-list-internal
Route::get('get-source-identifier-list-internal', 'APIController@getSourceIdentifierListInternal');

/*****************************************************
 *****************  External APIs  *******************
 ****************************************************/

// [POST] API to do license key activation
// Local: http://127.0.0.1:10001/activate-license-key
// Server: http://domain.com/public/activate-license-key
Route::post('activate-license-key', 'LicenseKeyController@activateLicenseKey');

// [POST] API for license key valdiation
// Local: http://127.0.0.1:10001/validate-license-key
// Server: http://domain.com/public/validate-license-key
Route::post('validate-license-key', 'LicenseKeyController@validateLicenseKey');

// [GET] API to generate registered player email list
// Local: http://127.0.0.1:10001/api/generate-player-email-list
// Server: http://domain.com/public/api/generate-player-email-list
Route::get('generate-player-email-list', 'APIController@generatePlayerEmailList');

// [GET] API to generate registered player email list downloadable URL
// Local: http://127.0.0.1:10001/api/download-player-email-list
// Server: http://domain.com/public/api/download-player-email-list
Route::get('download-player-email-list', 'APIController@downloadPlayerEmailList');

// [GET] API to generate used redeem code list
// Local: http://127.0.0.1:10001/api/generate-used-redeem-code-list
// Server: http://domain.com/public/api/generate-used-redeem-code-list
Route::get('generate-used-redeem-code-list', 'APIController@generateUsedRedeemCodeList');

// [GET] API to generate used redeem code list downloadable URL
// Local: http://127.0.0.1:10001/api/download-used-redeem-code-list
// Server: http://domain.com/public/api/download-used-redeem-code-list
Route::get('download-used-redeem-code-list', 'APIController@downloadUsedRedeemCodeList');

// [GET] API to trigger send emails
// Local: http://127.0.0.1:10001/api/send-voucher-email
// Server: http://domain.com/public/api/send-voucher-email
Route::get('send-voucher-email', 'APIController@sendVoucherEmail');

// [POST] API for player data submission
// Local: http://127.0.0.1:10001/api/submit-player-data
// Server: http://domain.com/public/api/submit-player-data
Route::post('submit-player-data', 'APIController@submitPlayerData');

// [POST] API for player duplicate registration checking
// Local: http://127.0.0.1:10001/api/check-duplicate-player
// Server: http://domain.com/public/api/check-duplicate-player
Route::post('check-duplicate-player', 'APIController@checkDuplicatePlayer');

// [POST] API for player duplicate registration checking
// Local: http://127.0.0.1:10001/api/check-duplicate-redeem-code
// Server: http://domain.com/public/api/check-duplicate-redeem-code
Route::post('check-duplicate-redeem-code', 'APIController@checkDuplicateRedeemCode');

// [POST] API for player's score data submission
// Local: http://127.0.0.1:10001/api/submit-score-data
// Server: http://domain.com/public/api/submit-score-data
Route::post('submit-score-data', 'APIController@submitScoreData');

// [POST] API for voucher request
// Local: http://127.0.0.1:10001/api/request-voucher
// Server: http://domain.com/public/api/request-voucher
Route::post('request-voucher', 'APIController@requestVoucher');

// [POST] API for voucher distribution data submission
// Local: http://127.0.0.1:10001/api/submit-voucher-data
// Server: http://domain.com/public/api/submit-voucher-data
Route::post('submit-voucher-data', 'APIController@submitVoucherDistributeData');

// [POST] API for daily stock reset
// Local: http://127.0.0.1:10001/api/daily-stock-reset
// Server: http://domain.com/public/api/daily-stock-reset
Route::post('daily-stock-reset', 'APIController@dailyStockReset');

// [GET] API for source identifier list
// Local: http://127.0.0.1:10001/api/get-source-identifier-list
// Server: http://domain.com/public/api/get-source-identifier-list
Route::get('get-source-identifier-list', 'APIController@getSourceIdentifierList');
