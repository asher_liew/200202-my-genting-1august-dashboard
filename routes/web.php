<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::resource('license_keys', 'LicenseKeyController');

Route::redirect('/', '/dashboard')->name('dashboard');

Route::get('/players', 'PlayerController@index');

Route::get('2FAForm', 'TwoFactorController@verifyForm')->name('2FAForm');

Route::post('2FAVerify', 'TwoFactorController@verify')->name('2FAVerify');

Route::get('/send-reference-mail', 'ReferenceController@sendReferenceEmail')->name('send-reference-mail');

Route::get('/dashboard', 'MainController@index')->name('dashboard');

Route::get('/scores', 'ScoreController@index');

Route::get('/shares', 'ShareController@index');

Route::get('/references', 'ReferenceController@index');

Route::get('/scores', 'ScoreController@index');

Route::get('/vouchers', 'VoucherDistributionController@index');

Route::get('/voucherOverview', 'VoucherDistributionController@voucherOverview');

Route::get('/getPageviews', 'GaController@getUserJourney');

Route::get('/redeemVoucher/outlet/{outlet}', 'VoucherController@redemption');

Route::get('/viewDeletedVouchers', 'VoucherController@getDeletedVouchers');

Route::get('/duplicateRedemption', 'VoucherController@duplicateRedemption');

// Defined for action of form id: restore-voucher-form
Route::post('restoreVoucher', 'VoucherController@restoreVoucher');

// Defined for action of form id: voucher-code-form
Route::post('voucherDetails', 'VoucherController@voucherDetails');

Route::resource('manageVouchers', 'VoucherController');

/*
 * Routes for overview APIs
*/
Route::get('/getDailyOverview', 'OverviewController@getDailyOverview');

Route::get('/getWeeklyOverview', 'OverviewController@getWeeklyOverview');

Route::get('/getBranchDailyOverview', 'OverviewController@getBranchDailyOverview');

Route::get('/getVoucherDailyOverview', 'VoucherDistributionController@getDailyOverview');

Route::get('/getRegistrationHourCount', 'OverviewController@getRegistrationHourCount');

/*
 * Routes for downloading CSV files
*/
Route::get('/downloadPlayersCSV', 'PlayerController@downloadCSV');
Route::get('/downloadUnredeemedVoucherCSV', 'VoucherDistributionController@downloadUnredeemedCSV');
Route::get('/downloadRedeemedVoucherCSV', 'VoucherDistributionController@downloadRedeemedCSV');
Route::get('/downloadReferencesCSV', 'ReferenceController@downloadCSV');
Route::get('/downloadScoresCSV', 'ScoreController@downloadCSV');
Route::get('/downloadSharesCSV', 'ShareController@downloadCSV');
Route::get('/downloadVouchersCSV', 'VoucherDistributionController@downloadCSV');
